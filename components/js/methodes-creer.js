import axios from 'axios'

export default {
	methods: {
		activerInput (id) {
			document.querySelector('#' + id).click()
		},
		definirTabIndex () {
			return this.$parent.modale === '' && this.$parent.message === '' && this.$parent.modaleConfirmation === '' && this.modale === '' ? 0 : -1
		},
		afficherAjouterMedia () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'ajouter-media'
			this.$emit('modale', true)
			this.$nextTick(function () {
				document.querySelector('#modale-ajouter-media input').focus()
			})
		},
		fermerModaleAjouterMedia () {
			this.modale = ''
			this.lien = ''
			this.$emit('modale', false)
			this.gererFocus()
		},
		ajouterVideo () {
			let id
			let lien
			// eslint-disable-next-line
			const regExpIframe = RegExp('<iframe(.+)</iframe>', 'g')
			// eslint-disable-next-line
			const regExpYT = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?[\w\?=]*)?/
			if (this.lien.includes('ladigitale.dev/digiview/inc/video.php') && regExpIframe.test(this.lien) === true) {
				lien = this.lien.match(/<iframe [^>]*src="[^"]*"[^>]*>/g).map(x => x.replace(/.*src="([^"]*)".*/, '$1'))[0]
				if (this.verifierURL(lien) === true) {
					id = this.lien.match(/videoId=(.*?)&vignette/)[1]
					this.support = { lien: lien, vignette: 'https://i.ytimg.com/vi/' + id + '/default.jpg', fichier: '', type: 'video' }
					this.fermerModaleAjouterMedia()
				}
			} else if (this.verifierURL(this.lien) === true && regExpYT.test(this.lien) === true) {
				id = this.lien.match(/^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|shorts\/|watch\?v=|&v=)([^#&?]*).*/)[2]
				this.support = { lien: 'https://www.youtube-nocookie.com/embed/' + id, vignette: 'https://i.ytimg.com/vi/' + id + '/default.jpg', fichier: '', type: 'video' }
				this.fermerModaleAjouterMedia()
			}
		},
		verifierURL (lien) {
			let url
			try {
				url = new URL(lien)
			} catch (_) {
				return false
			}
			return url.protocol === 'http:' || url.protocol === 'https:'
		},
		televerserMedia (event) {
			this.fermerModaleAjouterMedia()
			const champ = event.target
			const formats = ['jpg', 'jpeg', 'png', 'gif', 'mp3', 'wav', 'm4a', 'ogg']
			const extension = champ.files[0].name.substr(champ.files[0].name.lastIndexOf('.') + 1).toLowerCase()
			if (champ.files && champ.files[0] && formats.includes(extension) && champ.files[0].size < 10485760) {
				this.chargement = 'support'
				const fichier = champ.files[0]
				const formulaire = new FormData()
				formulaire.append('code', this.code)
				formulaire.append('fichier', fichier)
				axios.post(this.hote + '/api/televerser-media', formulaire, {
					headers: {
						'Content-Type': 'multipart/form-data'
					},
					onUploadProgress: function (progression) {
						const pourcentage = parseInt(Math.round((progression.loaded * 100) / progression.total))
						this.progression = pourcentage
					}.bind(this)
				}).then(function (reponse) {
					this.chargement = ''
					const donnees = reponse.data
					if (donnees === 'erreur') {
						this.message = this.$t('erreurCommunicationServeur')
					} else if (donnees === 'non_autorise') {
						this.message = this.$t('pasAutoriseModifierNuageDeMots')
					} else {
						this.support = donnees
						this.medias.push(donnees.fichier)
					}
					champ.value = ''
					this.progression = 0
				}.bind(this)).catch(function () {
					champ.value = ''
					this.chargement = ''
					this.progression = 0
					this.message = this.$t('erreurCommunicationServeur')
				}.bind(this))
			} else {
				if (!formats.includes(extension)) {
					this.message = this.$t('formatImageNonAccepte')
				} else if (champ.files[0].size >= 10485760) {
					this.message = this.$t('tailleMaximaleFichier')
				}
				champ.value = ''
			}
		},
		afficherMedia (media, type) {
			if (type === 'video') {
				this.media = { lien: media, type: type }
			} else {
				this.media = { fichier: media, type: type }
			}
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'media'
			this.$emit('modale', true)
			this.$nextTick(function () {
				document.querySelector('.modale .fermer').focus()
			})
		},
		supprimerMedia () {
			this.support = {}
			this.fermerModaleMedia()
		},
		fermerModaleMedia () {
			this.modale = ''
			this.media = {}
			this.$emit('modale', false)
			this.gererFocus()
		},
		gererFocus () {
			if (this.elementPrecedent) {
				this.elementPrecedent.focus()
				this.elementPrecedent = null
			}
		},
		gererClavier (event) {
			if (event.key === 'Escape' && this.modale === 'ajouter-media') {
				this.fermerModaleAjouterMedia()
			} else if (event.key === 'Escape' && this.modale === 'media') {
				this.fermerModaleMedia()
			} else if (event.key === 'Escape' && this.modale !== '') {
				this.modale = ''
				this.$emit('modale', false)
				this.gererFocus()
			}
		}
	}
}
