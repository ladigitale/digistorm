export default {
	methods: {
		activerInput (id) {
			document.querySelector('#' + id).click()
		},
		definirTabIndex () {
			return this.$parent.$parent.modale === '' && this.$parent.$parent.message === '' ? 0 : -1
		},
		definirReponse (event, indexQuestion) {
			if (this.questions[indexQuestion].option === 'choix-unique' || this.questions[indexQuestion].option === 'texte-court') {
				if (this.reponse[indexQuestion].length > 0) {
					this.reponse[indexQuestion].splice(0, 1)
				}
				this.reponse[indexQuestion].push(event.target.value)
			} else if (this.questions[indexQuestion].option === 'choix-multiples') {
				if (event.target.checked === true) {
					this.reponse[indexQuestion].push(event.target.value)
				} else {
					const index = this.reponse[indexQuestion].indexOf(event.target.value)
					this.reponse[indexQuestion].splice(index, 1)
				}
			}
		},
		definirReponseEnvoyee (indexQuestion) {
			let reponse = this.reponse[indexQuestion].toString()
			if (this.options.reponses === 'oui' && this.donneesSession[indexQuestion] && this.donneesSession[indexQuestion].reponseCorrecte === false) {
				reponse = '<s>' + this.reponse[indexQuestion].toString() + '</s> ' + this.donneesSession[indexQuestion].itemsCorrects.join(', ')
			}
			return reponse
		},
		modifierIndexQuestion () {
			this.$emit('index')
		},
		afficherSupport () {
			this.$emit('support')
		},
		afficherImage (event, image) {
			this.$emit('image', event, image)
		}
	}
}
