import 'dotenv/config'
import path from 'path'
import fs from 'fs-extra'
import express from 'express'
import { createServer } from 'http'
import { Server } from 'socket.io'
import { createAdapter } from '@socket.io/cluster-adapter'
import compression from 'compression'
import axios from 'axios'
import cors from 'cors'
import { createClient } from 'redis'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import v from 'voca'
import multer from 'multer'
import PDFDocument from 'pdfkit'
import sharp from 'sharp'
import archiver from 'archiver'
import extract from 'extract-zip'
import dayjs from 'dayjs'
import 'dayjs/locale/es.js'
import 'dayjs/locale/fr.js'
import 'dayjs/locale/it.js'
import 'dayjs/locale/de.js'
import localizedFormat from 'dayjs/plugin/localizedFormat.js'
import bcrypt from 'bcrypt'
import cron from 'node-cron'
import nodemailer from 'nodemailer'
import { fileURLToPath } from 'url'
import RedisStore from 'connect-redis'
import session from 'express-session'
import base64 from 'base-64'
import { NameForgeJS } from './nameforge.js'
import { renderPage } from 'vike/server'
// Charger strings langues
import t from './lang.js'

const production = process.env.NODE_ENV === 'production'
const cluster = parseInt(process.env.NODE_CLUSTER) === 1
const __dirname = path.dirname(fileURLToPath(import.meta.url))
const root = `${__dirname}/..`

// Vérifier si un buffer est une image jpg, png ou gif (pour PDFKit)
const magic = ['ffd8ffe0', '89504e47', '47494638']

demarrerServeur()

async function demarrerServeur () {
	const app = express()
	app.use(compression())
	const httpServer = createServer(app)

	let hote = 'http://localhost:3000'
	if (production) {
		hote = process.env.DOMAIN
	} else if (process.env.PORT) {
		hote = 'http://localhost:' + process.env.PORT
	}
	let db
	let db_port = 6379
	if (process.env.DB_PORT) {
		db_port = process.env.DB_PORT
	}
	if (production) {
		db = await createClient({
			url: 'redis://default:' + process.env.DB_PWD  + '@' + process.env.DB_HOST + ':' + db_port
		}).on('error', function (err) {
			console.log('redis: ', err)
		}).connect()
	} else {
		db = await createClient({
			url: 'redis://localhost:' + db_port
		}).on('error', function (err) {
			console.log('redis: ' + err)
		}).connect()
	}

	let storeOptions, cookie, dureeSession, domainesAutorises
	if (production) {
		storeOptions = {
			host: process.env.DB_HOST,
			port: db_port,
			pass: process.env.DB_PWD,
			client: db,
			prefix: 'sessions:'
		}
		cookie = {
			sameSite: 'None',
			secure: true
		}
	} else {
		storeOptions = {
			host: 'localhost',
			port: db_port,
			client: db,
			prefix: 'sessions:'
		}
		cookie = {
			secure: false
		}
	}
	const redisStore = new RedisStore(storeOptions)
	const sessionOptions = {
		secret: process.env.SESSION_KEY,
		store: redisStore,
		name: 'digistorm',
		resave: false,
		rolling: true,
		saveUninitialized: false,
		cookie: cookie
	}
	if (process.env.SESSION_DURATION) {
		dureeSession = parseInt(process.env.SESSION_DURATION)
	} else {
		dureeSession = 864000000 //3600 * 24 * 10 * 1000
	}
	const sessionMiddleware = session(sessionOptions)

	if (production && process.env.AUTHORIZED_DOMAINS) {
		domainesAutorises = process.env.AUTHORIZED_DOMAINS.split(',')
	} else {
		domainesAutorises = '*'
	}

	let earlyHints103 = false
	if (process.env.EARLY_HINTS && parseInt(process.env.EARLY_HINTS) === 1) {
		earlyHints103 = true
	}

	const transporter = nodemailer.createTransport({
		host: process.env.EMAIL_HOST,
		port: process.env.EMAIL_PORT,
		secure: process.env.EMAIL_SECURE,
		auth: {
			user: process.env.EMAIL_ADDRESS,
			pass: process.env.EMAIL_PASSWORD
		}
	})

	cron.schedule('59 23 * * Saturday', async function () {
		await fs.emptyDir(path.join(__dirname, '..', '/static/temp'))
	})

	// Charger plugin dayjs
	dayjs.extend(localizedFormat)

	app.set('trust proxy', true)
	app.use(
		helmet.contentSecurityPolicy({
			directives: {
				"default-src": ["'self'", "https:", "ws:", "data:"],
				"script-src": ["'self'", "'unsafe-inline'", "'unsafe-eval'", "blob:", 'https://cdn.jsdelivr.net'],
				"media-src": ["'self'", "https:", "data:"],
				"img-src": ["'self'", "https:", "data:"],
				"frame-ancestors": ["*"],
				"frame-src": ["*", "blob:"]
			}
		})
	)
	app.use(bodyParser.json({ limit: '50mb' }))
	app.use(sessionMiddleware)
	app.use(cors({ 'origin': domainesAutorises }))
	if (parseInt(process.env.REVERSE_PROXY) !== 1 || !production) {
		app.use('/', express.static('static'))
	}

	if (!production) {
		const vite = await import('vite')
    	const viteDevMiddleware = (
      		await vite.createServer({
        		root,
        		server: { middlewareMode: true }
			})
    	).middlewares
    	app.use(viteDevMiddleware)
  	} else if (production && parseInt(process.env.REVERSE_PROXY) !== 1) {
		const sirv = (await import('sirv')).default
		app.use(sirv(`${root}/dist/client`))
	}

	app.get('/', async function (req, res, next) {
		if (req.session.identifiant && req.session.role === 'utilisateur') {
			res.redirect('/u/' + req.session.identifiant)
		} else {
			let langue = 'fr'
			if (req.session.hasOwnProperty('langue') && req.session.langue !== '') {
				langue = req.session.langue
			}
			const pageContextInit = {
				urlOriginal: req.originalUrl,
				params: req.query,
				hote: hote,
				langues: ['fr', 'es', 'it', 'de', 'en'],
				langue: langue
			}
			const pageContext = await renderPage(pageContextInit)
			const { httpResponse } = pageContext
			if (!httpResponse) {
				return next()
			}
			const { body, statusCode, headers, earlyHints } = httpResponse
			if (earlyHints103 === true && res.writeEarlyHints) {
				res.writeEarlyHints({ link: earlyHints.map((e) => e.earlyHintLink) })
			}
			if (headers) {
				headers.forEach(([name, value]) => res.setHeader(name, value))
			}
			res.status(statusCode).send(body)
		}
	})

	app.get('/u/:utilisateur', async function (req, res, next) {
		const identifiant = req.params.utilisateur
		if (identifiant === req.session.identifiant && req.session.role === 'utilisateur') {
			recupererDonnees(identifiant).then(function (resultat) {
				const pageContextInit = {
					urlOriginal: req.originalUrl,
					params: req.query,
					hote: hote,
					langues: ['fr', 'es', 'it', 'de', 'en'],
					identifiant: req.session.identifiant,
					nom: req.session.nom,
					email: req.session.email,
					langue: req.session.langue,
					role: req.session.role,
					interactions: resultat[0],
					filtre: resultat[1]
				}
				envoyerPage(pageContextInit, res, next)
			})
		} else {
			res.redirect('/')
		}
	})

	app.get('/c/:code', async function (req, res, next) {
		if (req.query.id && req.query.id !== '' && req.query.mdp && req.query.mdp !== '') {
			await verifierAcces(req, parseInt(req.params.code), req.query.id, base64.decode(req.query.mdp))
		}
		if (!req.query.id && !req.query.mdp && (req.session.identifiant === '' || req.session.identifiant === undefined)) {
			const identifiant = 'u' + Math.random().toString(16).slice(3)
			req.session.identifiant = identifiant
			req.session.nom = ''
			req.session.email = ''
			req.session.langue = 'fr'
			req.session.role = 'invite'
			req.session.interactions = []
			req.session.cookie.expires = new Date(Date.now() + dureeSession)
		}
		if (!req.query.id && !req.query.mdp && !req.session.hasOwnProperty('interactions')) {
			req.session.interactions = []
		}
		let identifiant = req.session.identifiant
		const id = req.query.id
		const mdp = req.query.mdp
		if (id && id !== '' && mdp && mdp !== '') {
			identifiant = id
		}
		let pageContextInit = {}
		const code = parseInt(req.params.code)
		const reponse = await db.EXISTS('interactions:' + code)
		if (reponse === null) { 
			pageContextInit = {
				urlOriginal: req.originalUrl,
				erreur: true
			}
			envoyerPage(pageContextInit, res, next)
		} else if (reponse === 1) {
			let resultat = await db.HGETALL('interactions:' + code)
			resultat = Object.assign({}, resultat)
			if (resultat === null) { res.send('erreur'); return false }
			const type = resultat.type
			const titre = resultat.titre
			let proprietaire = ''
			let motdepasse = ''
			if (resultat.hasOwnProperty('identifiant')) {
				proprietaire = resultat.identifiant
			}
			let donnees = {}
			let reponses = []
			let sessions = []
			const statut = resultat.statut
			const session = parseInt(resultat.session)
			let digidrive = 0
			if ((req.session.role === 'auteur' && req.session.interactions.map(item => item.code).includes(code)) || (req.session.role === 'utilisateur' && proprietaire === identifiant)) {
				if (resultat.hasOwnProperty('motdepasse')) {
					motdepasse = resultat.motdepasse
				}
				donnees = JSON.parse(resultat.donnees)
				reponses = JSON.parse(resultat.reponses)
				sessions = JSON.parse(resultat.sessions)
				if (req.session.role === 'auteur' && resultat.hasOwnProperty('digidrive')) {
					digidrive = resultat.digidrive
				}
			} else if (proprietaire !== identifiant) {
				proprietaire = ''
			}
			pageContextInit = {
				urlOriginal: req.originalUrl,
				params: req.query,
				hote: hote,
				langues: ['fr', 'es', 'it', 'de', 'en'],
				identifiant: req.session.identifiant,
				nom: req.session.nom,
				email: req.session.email,
				langue: req.session.langue,
				role: req.session.role,
				interactions: req.session.interactions,
				type: type,
				titre: titre,
				proprietaire: proprietaire,
				motdepasse: motdepasse,
				donnees: donnees,
				reponses: reponses,
				sessions: sessions,
				statut: statut,
				session: session,
				digidrive: digidrive
			}
			envoyerPage(pageContextInit, res, next)
		} else {
			pageContextInit = {
				urlOriginal: req.originalUrl,
				erreur: true
			}
			envoyerPage(pageContextInit, res, next)
		}
	})

	app.get('/p/:code', async function (req, res, next) {
		if (req.session.identifiant === '' || req.session.identifiant === undefined) {
			const identifiant = 'u' + Math.random().toString(16).slice(3)
			req.session.identifiant = identifiant
			req.session.nom = ''
			req.session.email = ''
			req.session.langue = 'fr'
			req.session.role = 'invite'
			req.session.interactions = []
			req.session.cookie.expires = new Date(Date.now() + dureeSession)
		}
		if (!req.session.hasOwnProperty('interactions')) {
			req.session.interactions = []
		}
		const pageContextInit = {
			urlOriginal: req.originalUrl,
			params: req.query,
			hote: hote,
			langues: ['fr', 'es', 'it', 'de', 'en'],
			identifiant: req.session.identifiant,
			nom: req.session.nom,
			langue: req.session.langue
		}
		const pageContext = await renderPage(pageContextInit)
		const { httpResponse } = pageContext
		if (!httpResponse) {
			return next()
		}
		const { body, statusCode, headers, earlyHints } = httpResponse
		if (earlyHints103 === true && res.writeEarlyHints) {
			res.writeEarlyHints({ link: earlyHints.map((e) => e.earlyHintLink) })
		}
		if (headers) {
			headers.forEach(([name, value]) => res.setHeader(name, value))
		}
		res.status(statusCode).send(body)
	})

	app.get('/admin', async function (req, res, next) {
		let langue = 'fr'
		if (req.session.hasOwnProperty('langue') && req.session.langue !== '') {
			langue = req.session.langue
		}
		const pageContextInit = {
			urlOriginal: req.originalUrl,
			hote: hote,
			langue: langue
		}
		const pageContext = await renderPage(pageContextInit)
		const { httpResponse } = pageContext
		if (!httpResponse) {
			return next()
		}
		const { body, statusCode, headers, earlyHints } = httpResponse
		if (earlyHints103 === true && res.writeEarlyHints) {
			res.writeEarlyHints({ link: earlyHints.map((e) => e.earlyHintLink) })
		}
		if (headers) {
			headers.forEach(([name, value]) => res.setHeader(name, value))
		}
		res.status(statusCode).send(body)
  	})

	app.post('/api/s-inscrire', async function (req, res) {
		const identifiant = req.body.identifiant
		const motdepasse = req.body.motdepasse
		const email = req.body.email
		let langue = 'fr'
		if (req.session && req.session.hasOwnProperty('langue') && req.session.langue !== '') {
			langue = req.session.langue
		}
		const reponse = await db.EXISTS('utilisateurs:' + identifiant)
		if (reponse === null) { res.send('erreur') }
		if (reponse === 0) {
			const hash = await bcrypt.hash(motdepasse, 10)
			const date = dayjs().format()
			await db.HSET('utilisateurs:' + identifiant, ['id', identifiant, 'email', email, 'motdepasse', hash, 'date', date, 'nom', '', 'langue', langue])
			req.session.identifiant = identifiant
			req.session.nom = ''
			req.session.email = email
			if (req.session.langue === '' || req.session.langue === undefined) {
				req.session.langue = 'fr'
			}
			req.session.role = 'utilisateur'
			req.session.cookie.expires = new Date(Date.now() + dureeSession)
			res.json({ identifiant: identifiant })
		} else {
			res.send('utilisateur_existe_deja')
		}
	})

	app.post('/api/se-connecter', async function (req, res) {
		const identifiant = req.body.identifiant
		const motdepasse = req.body.motdepasse
		const reponse = await db.EXISTS('utilisateurs:' + identifiant)
		if (reponse === null) { res.send('erreur_connexion'); return false }
		if (reponse === 1) {
			let donnees = await db.HGETALL('utilisateurs:' + identifiant)
			donnees = Object.assign({}, donnees)
			if (donnees === null) { res.send('erreur_connexion'); return false }
			let comparaison = false
			if (motdepasse.trim() !== '' && donnees.hasOwnProperty('motdepasse') && donnees.motdepasse.trim() !== '') {
				comparaison = await bcrypt.compare(motdepasse, donnees.motdepasse)
			}
			let comparaisonTemp = false
			if (donnees.hasOwnProperty('motdepassetemp') && donnees.motdepassetemp.trim() !== '' && motdepasse.trim() !== '') {
				comparaisonTemp = await bcrypt.compare(motdepasse, donnees.motdepassetemp)
			}
			if (comparaison === true || comparaisonTemp === true) {
				if (comparaisonTemp === true) {
					const hash = await bcrypt.hash(motdepasse, 10)
					await db.HSET('utilisateurs:' + identifiant, 'motdepasse', hash)
					await db.HDEL('utilisateurs:' + identifiant, 'motdepassetemp')
				}
				const nom = donnees.nom
				const langue = donnees.langue
				req.session.identifiant = identifiant
				req.session.nom = nom
				req.session.langue = langue
				req.session.role = 'utilisateur'
				req.session.cookie.expires = new Date(Date.now() + dureeSession)
				let email = ''
				if (donnees.hasOwnProperty('email')) {
					email = donnees.email
				}
				req.session.email = email
				res.json({ identifiant: identifiant })
			} else {
				res.send('erreur_connexion')
			}
		} else {
			res.send('erreur_connexion')
		}
	})

	app.post('/api/mot-de-passe-oublie', async function (req, res) {
		const identifiant = req.body.identifiant
		let email = req.body.email.trim()
		const reponse = await db.EXISTS('utilisateurs:' + identifiant)
		if (reponse === null) { res.send('erreur'); return false }
		if (reponse === 1) {
			let donnees = await db.HGETALL('utilisateurs:' + identifiant)
			donnees = Object.assign({}, donnees)
			if (donnees === null) { res.send('erreur'); return false }
			if ((donnees.hasOwnProperty('email') && donnees.email === email) || (verifierEmail(identifiant) === true)) {
				if (!donnees.hasOwnProperty('email') || (donnees.hasOwnProperty('email') && donnees.email === '')) {
					email = identifiant
				}
				const motdepasse = genererMotDePasse(7)
				const message = {
					from: '"La Digitale" <' + process.env.EMAIL_ADDRESS + '>',
					to: '"Moi" <' + email + '>',
					subject: 'Mot de passe Digistorm',
					html: '<p>Votre nouveau mot de passe : ' + motdepasse + '</p>'
				}
				transporter.sendMail(message, async function (err) {
					if (err) {
						res.send('erreur')
					} else {
						const hash = await bcrypt.hash(motdepasse, 10)
						await db.HSET('utilisateurs:' + identifiant, 'motdepassetemp', hash)
						res.send('message_envoye')
					}
				})
			} else {
				res.send('email_invalide')
			}
		} else {
			res.send('identifiant_invalide')
		}
	})

	app.post('/api/se-deconnecter', function (req, res) {
		req.session.identifiant = ''
		req.session.nom = ''
		req.session.email = ''
		req.session.langue = ''
		req.session.role = ''
		req.session.interactions = []
		req.session.destroy()
		res.send('deconnecte')
	})

	app.post('/api/modifier-langue', function (req, res) {
		const langue = req.body.langue
		req.session.langue = langue
		res.send('langue_modifiee')
	})

	app.post('/api/modifier-nom', function (req, res) {
		const nom = req.body.nom
		req.session.nom = nom
		res.send('nom_modifie')
	})

	app.post('/api/generer-nom', function (req, res) {
		const generateur = new NameForgeJS()
		const noms = generateur.generateNames()
		const nom = noms[0].replace(/(^\w{1})|(\s+\w{1})/g, lettre => lettre.toUpperCase())
		req.session.nom = nom
		res.send(nom)
	})

	app.post('/api/modifier-filtre', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant && req.session.role === 'utilisateur') {
			const filtre = req.body.filtre
			await db.HSET('utilisateurs:' + identifiant, 'filtre', filtre)
			res.send('filtre_modifie')
		} else {
			res.send('non_connecte')
		}
	})

	app.post('/api/modifier-informations-utilisateur', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant && req.session.role === 'utilisateur') {
			const nom = req.body.nom
			const email = req.body.email
			await db.HSET('utilisateurs:' + identifiant, ['nom', nom, 'email', email])
			req.session.nom = nom
			req.session.email = email
			res.send('utilisateur_modifie')
		} else {
			res.send('non_connecte')
		}
	})

	app.post('/api/modifier-mot-de-passe-utilisateur', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant && req.session.role === 'utilisateur') {
			let donnees = await db.HGETALL('utilisateurs:' + identifiant)
			donnees = Object.assign({}, donnees)
			if (donnees === null) { res.send('erreur'); return false }
			const motdepasse = req.body.motdepasse
			const nouveaumotdepasse = req.body.nouveaumotdepasse
			if (motdepasse.trim() !== '' && nouveaumotdepasse.trim() !== '' && donnees.hasOwnProperty('motdepasse') && donnees.motdepasse.trim() !== '' && await bcrypt.compare(motdepasse, donnees.motdepasse)) {
				const hash = await bcrypt.hash(nouveaumotdepasse, 10)
				await db.HSET('utilisateurs:' + identifiant, 'motdepasse', hash)
				res.send('motdepasse_modifie')
			} else {
				res.send('motdepasse_incorrect')
			}
		} else {
			res.send('non_connecte')
		}
	})

	app.post('/api/verifier-mot-de-passe-admin', function (req, res) {
		const admin = req.body.admin
		if (admin !== '' && admin === process.env.VITE_ADMIN_PASSWORD) {
			res.send('acces_verifie')
		} else {
			res.send('acces_invalide')
		}
	})

	app.post('/api/modifier-mot-de-passe-admin', async function (req, res) {
		const admin = req.body.admin
		if (admin !== '' && admin === process.env.VITE_ADMIN_PASSWORD) {
			const identifiant = req.body.identifiant
			const email = req.body.email
			if (identifiant !== '') {
				const resultat = await db.EXISTS('utilisateurs:' + identifiant)
				if (resultat === null) { res.send('erreur'); return false }
				if (resultat === 1) {
					const hash = await bcrypt.hash(req.body.motdepasse, 10)
					await db.HSET('utilisateurs:' + identifiant, 'motdepasse', hash)
					res.send('motdepasse_modifie')
				} else {
					res.send('identifiant_non_valide')
				}
			} else if (email !== '') {
				const utilisateurs = await db.KEYS('utilisateurs:*')
				if (utilisateurs !== null) {
					const donneesUtilisateurs = []
					utilisateurs.forEach(function (utilisateur) {
						const donneesUtilisateur = new Promise(async function (resolve) {
							let donnees = await db.HGETALL('utilisateurs:' + utilisateur.substring(13))
							donnees = Object.assign({}, donnees)
							if (donnees === null) { resolve({}); return false }
							if (donnees.hasOwnProperty('email')) {
								resolve({ identifiant: utilisateur.substring(13), email: donnees.email })
							} else {
								resolve({})
							}
						})
						donneesUtilisateurs.push(donneesUtilisateur)
					})
					Promise.all(donneesUtilisateurs).then(async function (donnees) {
						let utilisateurId = ''
						donnees.forEach(function (utilisateur) {
							if (utilisateur.hasOwnProperty('email') && utilisateur.email.toLowerCase() === email.toLowerCase()) {
								utilisateurId = utilisateur.identifiant
							}
						})
						if (utilisateurId !== '') {
							const hash = await bcrypt.hash(req.body.motdepasse, 10)
							await db.HSET('utilisateurs:' + utilisateurId, 'motdepasse', hash)
							res.send(utilisateurId)
						} else {
							res.send('email_non_valide')
						}
					})
				} else {
					res.send('erreur')
				}
			}
		}
	})

	app.post('/api/recuperer-donnees-interaction-admin', async function (req, res) {
		const code = parseInt(req.body.code)
		const admin = req.body.admin
		if (admin !== '' && admin === process.env.VITE_ADMIN_PASSWORD) {
			const resultat = await db.EXISTS('interactions:' + code)
			if (resultat === null) { res.send('erreur'); return false }
			if (resultat === 1) {
				let donnees = await db.HGETALL('interactions:' + code)
				donnees = Object.assign({}, donnees)
				if (donnees === null) { res.send('erreur'); return false }
				res.json(donnees)
			} else {
				res.send('interaction_inexistante')
			}
		}
	})

	app.post('/api/modifier-donnees-interaction-admin', async function (req, res) {
		const code = parseInt(req.body.code)
		const champ = req.body.champ
		const valeur = req.body.valeur
		const admin = req.body.admin
		if (admin !== '' && admin === process.env.VITE_ADMIN_PASSWORD) {
			const resultat = await db.EXISTS('interactions:' + code)
			if (resultat === null) { res.send('erreur'); return false }
			if (resultat === 1) {
				await db.HSET('interactions:' + code, champ, valeur)
				res.send('donnees_modifiees')
			} else {
				res.send('interaction_inexistante')
			}
		}
	})

	app.post('/api/modifier-langue-utilisateur', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const langue = req.body.langue
			await db.HSET('utilisateurs:' + identifiant, 'langue', langue)
			req.session.langue = langue
			res.send('langue_modifiee')
		} else {
			res.send('non_connecte')
		}
	})

	app.post('/api/supprimer-compte', async function (req, res) {
		const identifiant = req.body.identifiant
		const admin = req.body.admin
		const motdepasseAdmin = process.env.VITE_ADMIN_PASSWORD
		let type = 'utilisateur'
		if ((req.session.identifiant && req.session.identifiant === identifiant && req.session.role === 'utilisateur') || (admin !== '' && admin === motdepasseAdmin)) {
			if (admin === motdepasseAdmin) {
				type === 'admin'
			}
			const interactions = await db.SMEMBERS('interactions-creees:' + identifiant)
			if (interactions === null) { res.send('erreur'); return false }
			const donneesInteractions = []
			for (const interaction of interactions) {
				const donneesInteraction = new Promise(async function (resolve) {
					await db.DEL('interactions:' + interaction)
					const chemin = path.join(__dirname, '..', '/static/fichiers/' + interaction)
					if (await fs.pathExists(chemin)) {
						await fs.remove(chemin)
						resolve(interaction)
					} else {
						resolve(interaction)
					}
				})
				donneesInteractions.push(donneesInteraction)
			}
			Promise.all(donneesInteractions).then(async function () {
				await db
				.multi()
				.DEL('interactions-creees:' + identifiant)
				.DEL('utilisateurs:' + identifiant)
				.exec()
				if (type === 'utilisateur') {
					req.session.identifiant = ''
					req.session.nom = ''
					req.session.email = ''
					req.session.langue = ''
					req.session.role = ''
					req.session.interactions = []
					req.session.destroy()
					res.send('compte_supprime')
				} else {
					const sessions = await db.KEYS('sessions:*')
					if (sessions !== null) {
						const donneesSessions = []
						sessions.forEach(function (session) {
							const donneesSession = new Promise(async function (resolve) {
								let donnees = await db.GET('sessions:' + session.substring(9))
								if (donnees === null) { resolve({}); return false }
								donnees = JSON.parse(donnees)
								if (donnees.hasOwnProperty('identifiant')) {
									resolve({ session: session.substring(9), identifiant: donnees.identifiant })
								} else {
									resolve({})
								}
							})
							donneesSessions.push(donneesSession)
						})
						Promise.all(donneesSessions).then(async function (donnees) {
							let sessionId = ''
							donnees.forEach(function (item) {
								if (item.hasOwnProperty('identifiant') && item.identifiant === identifiant) {
									sessionId = item.session
								}
							})
							if (sessionId !== '') {
								await db.DEL('sessions:' + sessionId)
							}
							res.send('compte_supprime')
						})
					} else {
						res.send('erreur')
					}
				}
			})
		} else {
			res.send('non_connecte')
		}
	})

	app.post('/api/rejoindre-interaction', async function (req, res) {
		const code = parseInt(req.body.code)
		const reponse = await db.EXISTS('interactions:' + code)
		if (reponse === null) { res.send('erreur'); return false }
		if (reponse === 1) {
			if (req.session.identifiant === '' || req.session.identifiant === undefined) {
				const identifiant = 'u' + Math.random().toString(16).slice(3)
				req.session.identifiant = identifiant
				req.session.nom = ''
				req.session.email = ''
				req.session.langue = 'fr'
				req.session.role = 'invite'
				req.session.interactions = []
				req.session.cookie.expires = new Date(Date.now() + dureeSession)
			}
			res.json({ code: code, identifiant: req.session.identifiant })
		} else {
			res.send('erreur_code')
		}
	})

	app.post('/api/creer-interaction', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const titre = req.body.titre
			const type = req.body.type
			const code = Math.floor(1000000 + Math.random() * 9000000)
			const date = dayjs().format()
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 0) {
				await db
				.multi()
				.HSET('interactions:' + code, ['type', type, 'titre', titre, 'code', code, 'identifiant', identifiant, 'motdepasse', '', 'donnees', JSON.stringify({}), 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 0])
				.SADD('interactions-creees:' + identifiant, code.toString())
				.exec()
				const chemin = path.join(__dirname, '..', '/static/fichiers/' + code)
				await fs.mkdirp(chemin)
				res.json({ code: code })
			} else {
				res.send('existe_deja')
			}
		} else {
			res.send('non_connecte')
		}
	})

	app.post('/api/creer-interaction-sans-compte', async function (req, res) {
		if (req.session.identifiant === '' || req.session.identifiant === undefined || (req.session.identifiant.length !== 13 && req.session.identifiant.substring(0, 1) !== 'u')) {
			const identifiant = 'u' + Math.random().toString(16).slice(3)
			req.session.identifiant = identifiant
		}
		if (!req.session.hasOwnProperty('interactions')) {
			req.session.interactions = []
		}
		const titre = req.body.titre
		const type = req.body.type
		const code = Math.floor(1000000 + Math.random() * 9000000)
		const motdepasse = creerMotDePasse()
		const date = dayjs().format()
		const reponse = await db.EXISTS('interactions:' + code)
		if (reponse === null) { res.send('erreur'); return false }
		if (reponse === 0) {
			await db.HSET('interactions:' + code, ['type', type, 'titre', titre, 'code', code, 'motdepasse', motdepasse, 'donnees', JSON.stringify({}), 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 0])
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + code)
			await fs.mkdirp(chemin)
			req.session.nom = ''
			req.session.email = ''
			if (req.session.langue === '' || req.session.langue === undefined) {
				req.session.langue = 'fr'
			}
			req.session.role = 'auteur'
			req.session.interactions.push({ code: code, motdepasse: motdepasse })
			req.session.cookie.expires = new Date(Date.now() + dureeSession)
			res.json({ code: code })
		} else {
			res.send('existe_deja')
		}
	})

	app.post('/api/modifier-interaction', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const code = parseInt(req.body.code)
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { res.send('erreur'); return false }
				if ((resultat.hasOwnProperty('motdepasse') && resultat.motdepasse !== req.body.motdepasse) || (resultat.hasOwnProperty('identifiant') && resultat.identifiant !== identifiant)) {
					res.send('non_autorise')
					return false
				}
				const titre = req.body.titre
				const type = resultat.type
				const donnees = req.body.donnees
				const donneesActuelles = JSON.parse(resultat.donnees)
				const fichiersActuels = []
				const fichiers = []
				const corbeille = []
				if (Object.keys(donneesActuelles).length > 0) {
					if (Object.keys(donneesActuelles.support).length > 0) {
						if (donneesActuelles.support.hasOwnProperty('fichier')) {
							fichiersActuels.push(donneesActuelles.support.fichier)
						} else if (donneesActuelles.support.hasOwnProperty('image')) {
							fichiersActuels.push(donneesActuelles.support.image)
						}
					}
					if (type === 'Sondage' || type === 'Questionnaire') {
						if (donneesActuelles.hasOwnProperty('questions')) {
							donneesActuelles.questions.forEach(function (q) {
								q.items.forEach(function (item) {
									if (item.image !== '') {
										fichiersActuels.push(item.image)
									}
								})
							})
						} else {
							donneesActuelles.items.forEach(function (item) {
								if (item.image !== '') {
									fichiersActuels.push(item.image)
								}
							})
						}
					} else if (type === 'Remue-méninges') {
						donneesActuelles.categories.forEach(function (categorie) {
							if (categorie.image !== '') {
								fichiersActuels.push(categorie.image)
							}
						})
					}
					if (Object.keys(donnees.support).length > 0) {
						if (donnees.support.hasOwnProperty('fichier')) {
							fichiers.push(donnees.support.fichier)
						} else if (donnees.support.hasOwnProperty('image')) {
							fichiers.push(donnees.support.image)
						}
					}
					if (type === 'Sondage' || type === 'Questionnaire') {
						if (donnees.hasOwnProperty('questions')) {
							donnees.questions.forEach(function (q) {
								q.items.forEach(function (item) {
									if (item.image !== '') {
										fichiers.push(item.image)
									}
								})
							})
						} else {
							donnees.items.forEach(function (item) {
								if (item.image !== '') {
									fichiers.push(item.image)
								}
							})
						}
					} else if (type === 'Remue-méninges') {
						donnees.categories.forEach(function (categorie) {
							if (categorie.image !== '') {
								fichiers.push(categorie.image)
							}
						})
					}
					fichiersActuels.forEach(function (fichier) {
						if (!fichiers.includes(fichier)) {
							corbeille.push(fichier)
						}
					})
				}
				await db.HSET('interactions:' + code, ['titre', titre, 'donnees', JSON.stringify(donnees)])
				if (corbeille.length > 0) {
					corbeille.forEach(function (fichier) {
						supprimerFichier(code, fichier)
					})
				}
				res.send('donnees_enregistrees')
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/modifier-statut-interaction', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const code = parseInt(req.body.code)
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				const statut = req.body.statut
				if (statut === 'ouvert') {
					let resultat = await db.HGETALL('interactions:' + code)
					resultat = Object.assign({}, resultat)
					if (resultat === null) { res.send('erreur'); return false }
					if ((resultat.hasOwnProperty('motdepasse') && resultat.motdepasse !== req.body.motdepasse) || (resultat.hasOwnProperty('identifiant') && resultat.identifiant !== identifiant)) {
						res.send('non_autorise')
						return false
					}
					const date = dayjs().format()
					const session = resultat.session
					const sessions = JSON.parse(resultat.sessions)
					sessions[session] = {}
					sessions[session].debut = date
					await db.HSET('interactions:' + code, ['statut', statut, 'sessions', JSON.stringify(sessions)])
					res.send('statut_modifie')
				} else {
					await db.HSET('interactions:' + code, 'statut', statut)
					res.send('statut_modifie')
				}
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/modifier-index-question', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const code = parseInt(req.body.code)
			const indexQuestion = req.body.indexQuestion
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { res.send('erreur'); return false }
				if ((resultat.hasOwnProperty('motdepasse') && resultat.motdepasse !== req.body.motdepasse) || (resultat.hasOwnProperty('identifiant') && resultat.identifiant !== identifiant)) {
					res.send('non_autorise')
					return false
				}
				const donnees = JSON.parse(resultat.donnees)
				donnees.indexQuestion = indexQuestion
				await db.HSET('interactions:' + code, 'donnees', JSON.stringify(donnees))
				res.send('index_modifie')
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/fermer-interaction', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const code = parseInt(req.body.code)
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { res.send('erreur'); return false }
				if ((resultat.hasOwnProperty('motdepasse') && resultat.motdepasse !== req.body.motdepasse) || (resultat.hasOwnProperty('identifiant') && resultat.identifiant !== identifiant)) {
					res.send('non_autorise')
					return false
				}
				const date = dayjs().format()
				let session = resultat.session
				const type = resultat.type
				const donnees = JSON.parse(resultat.donnees)
				const reponses = JSON.parse(resultat.reponses)
				const sessions = JSON.parse(resultat.sessions)
				if (reponses[session] && reponses[session].length > 0 && sessions[session]) {
					sessions[session].fin = date
					sessions[session].donnees = donnees
					if (type === 'Questionnaire') {
						sessions[session].classement = req.body.classement
					}
				} else if (sessions[session]) {
					delete sessions[session]
				}
				session = parseInt(session) + 1
				if (type === 'Questionnaire') {
					donnees.indexQuestion = donnees.copieIndexQuestion
					await db.HSET('interactions:' + code, ['statut', 'termine', 'donnees', JSON.stringify(donnees), 'sessions', JSON.stringify(sessions), 'session', session])
					res.json({ session: session, reponses: reponses, sessions: sessions })
				} else {
					await db.HSET('interactions:' + code, ['statut', 'termine', 'sessions', JSON.stringify(sessions), 'session', session])
					res.json({ session: session, reponses: reponses, sessions: sessions })
				}
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/se-connecter-interaction', async function (req, res) {
		if (req.session.identifiant === '' || req.session.identifiant === undefined) {
			const identifiant = 'u' + Math.random().toString(16).slice(3)
			req.session.identifiant = identifiant
		}
		if (!req.session.hasOwnProperty('interactions')) {
			req.session.interactions = []
		}
		const code = parseInt(req.body.code)
		const motdepasse = req.body.motdepasse
		const reponse = await db.EXISTS('interactions:' + code)
		if (reponse === null) { res.send('erreur'); return false }
		if (reponse === 1) {
			let resultat = await db.HGETALL('interactions:' + code)
			resultat = Object.assign({}, resultat)
			if (resultat === null) { res.send('erreur'); return false }
			if (motdepasse !== '' && motdepasse === resultat.motdepasse) {
				let digidrive = 0
				if (resultat.hasOwnProperty('digidrive')) {
					digidrive = resultat.digidrive
				}
				req.session.nom = ''
				req.session.email = ''
				if (req.session.langue === '' || req.session.langue === undefined) {
					req.session.langue = 'fr'
				}
				req.session.role = 'auteur'
				req.session.cookie.expires = new Date(Date.now() + dureeSession)
				req.session.interactions.push({ code: code, motdepasse: motdepasse })
				res.json({ code: code, identifiant: req.session.identifiant, nom: '', role: 'auteur', interactions: req.session.interactions, donnees: resultat.donnees, reponses: resultat.reponses, sessions: resultat.sessions, digidrive: digidrive })
			} else {
				res.send('non_autorise')
			}
		} else {
			res.send('erreur_code')
		}
	})

	app.post('/api/recuperer-donnees-interaction-utilisateur', async function (req, res) {
		const code = parseInt(req.body.code)
		const identifiant = req.body.identifiant
		const reponse = await db.EXISTS('interactions:' + code)
		if (reponse === null) { res.send('erreur'); return false }
		if (reponse === 1) {
			let resultat = await db.HGETALL('interactions:' + code)
			resultat = Object.assign({}, resultat)
			if (resultat === null) { res.send('erreur'); return false }
			const type = resultat.type
			const titre = resultat.titre
			const statut = resultat.statut
			const session = parseInt(resultat.session)
			let donnees = {}
			let reponsesUtilisateurs = []
			const reponsesSession = []
			const donneesSession = []
			let scoreTotal = 0
			let nomObligatoire = false
			let nomAleatoire = false
			const donneesJSON = JSON.parse(resultat.donnees)
			if (donneesJSON && donneesJSON.hasOwnProperty('options') && donneesJSON.options.hasOwnProperty('nom') && donneesJSON.options.nom === 'obligatoire') {
				nomObligatoire = true
			} else if (donneesJSON && donneesJSON.hasOwnProperty('options') && donneesJSON.options.hasOwnProperty('nom') && donneesJSON.options.nom === 'aleatoire') {
				nomAleatoire = true
			}
			if (statut === 'ouvert' || statut === 'nuage-affiche' || statut === 'verrouille') {
				donnees = JSON.parse(resultat.donnees)
				const reponses = JSON.parse(resultat.reponses)
				if (reponses[session]) {
					reponsesUtilisateurs = reponses[session]
				}
				reponsesUtilisateurs.forEach(function (item) {
					if (item.identifiant === identifiant) {
						reponsesSession.push(item)
					}
				})
				if (type === 'Questionnaire') {
					if (reponsesSession[0] && reponsesSession[0].reponse) {
						reponsesSession[0].reponse.forEach(function (item, index) {
							const question = donnees.questions[index]
							const reponseCorrecte = definirReponseCorrecte(question, item).reponseCorrecte
							let itemsCorrects = []
							if (donnees.options.reponses === true || donnees.options.reponses === 'oui') {
								itemsCorrects = definirReponseCorrecte(question, item).itemsCorrects
							} else if (donnees.options.reponses === 'utilisateur') {
								itemsCorrects = definirReponseCorrecte(question, item).itemsCorrects
								itemsCorrects = itemsCorrects.filter(function (element) {
									return item.includes(element)
								})
							}
							let retroaction = ''
							if (donnees.options.retroaction === true && reponseCorrecte && question.hasOwnProperty('retroaction') && question.retroaction.correcte !== '') {
								retroaction = question.retroaction.correcte
							} else if (donnees.options.retroaction === true && !reponseCorrecte && question.hasOwnProperty('retroaction') && question.retroaction.incorrecte !== '') {
								retroaction = question.retroaction.incorrecte
							}
							donneesSession.push({ reponseCorrecte: reponseCorrecte, itemsCorrects: itemsCorrects, retroaction: retroaction })
						})
						scoreTotal = calculerScoreTotal(reponsesSession[0], donnees.options, donnees.questions)
					}
					donnees.questions.forEach(function (question) {
						if (question.hasOwnProperty('reponses')) {
							delete question.reponses
						}
						if (question.hasOwnProperty('retroaction')) {
							delete question.retroaction
						}
						question.items.forEach(function (item) {
							if (item.hasOwnProperty('reponse')) {
								delete item.reponse
							}
						})
					})
				}
			}
			res.json({ type: type, titre: titre, donnees: donnees, reponsesSession: reponsesSession, donneesSession: donneesSession, statut: statut, session: session, scoreTotal: scoreTotal, nomObligatoire: nomObligatoire, nomAleatoire: nomAleatoire })
		} else {
			res.send('interaction_inexistante')
		}
	})

	app.post('/api/telecharger-informations-interaction', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const code = parseInt(req.body.code)
			const motdepasse = req.body.motdepasse
			const type = req.body.type
			const titre = req.body.titre
			const domaine = req.body.domaine
			const doc = new PDFDocument()
			const fichier = code + '_' + Math.random().toString(36).substring(2, 12) + '.pdf'
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier)
			if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + code))) {
				const flux = fs.createWriteStream(chemin)
				doc.pipe(flux)
				doc.fontSize(16)
				if (type === 'Sondage') {
					doc.font('Helvetica-Bold').text(t[req.session.langue].sondage + ' - ' + titre)
				} else if (type === 'Questionnaire') {
					doc.font('Helvetica-Bold').text(t[req.session.langue].questionnaire + ' - ' + titre)
				} else if (type === 'Remue-méninges') {
					doc.font('Helvetica-Bold').text(t[req.session.langue].remueMeninges + ' - ' + titre)
				} else if (type === 'Nuage-de-mots') {
					doc.font('Helvetica-Bold').text(t[req.session.langue].nuageDeMots + ' - ' + titre)
				}
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica').text(t[req.session.langue].code + ' ' + code)
				doc.moveDown()
				doc.font('Helvetica').text(t[req.session.langue].lien).text(domaine + '/p/' + code, {
					link: domaine + '/p/' + code,
					underline: true
				})
				doc.moveDown()
				doc.font('Helvetica').text(t[req.session.langue].lienAdmin).text(domaine + '/c/' + code, {
					link: domaine + '/c/' + code,
					underline: true
				})
				doc.moveDown()
				doc.font('Helvetica').text(t[req.session.langue].motdepasse + ' ' + motdepasse)
				doc.moveDown()
				doc.end()
				flux.on('finish', function () {
					res.send(fichier)
				})
			} else {
				res.send('erreur')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/supprimer-informations-interaction', function (req, res) {
		const code = parseInt(req.body.code)
		const fichier = req.body.fichier
		supprimerFichier(code, fichier)
		res.send('fichier_supprime')
	})

	app.post('/api/dupliquer-interaction', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant && req.session.role === 'utilisateur') {
			const interaction = parseInt(req.body.code)
			const reponse = await db.EXISTS('interactions:' + interaction)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				let parametres = await db.HGETALL('interactions:' + interaction)
				parametres = Object.assign({}, parametres)
				if (parametres === null) { res.send('erreur'); return false }
				if (parametres.identifiant !== identifiant) {
					res.send('non_autorise')
					return false
				}
				const code = Math.floor(1000000 + Math.random() * 9000000)
				const date = dayjs().format()
				let resultat = await db.EXISTS('interactions:' + code)
				if (resultat === null) { res.send('erreur'); return false }
				if (resultat === 0) {
					await db
					.multi()
					.HSET('interactions:' + code, ['type', parametres.type, 'titre', t[req.session.langue].copieDe + parametres.titre, 'code', code, 'identifiant', identifiant, 'motdepasse', '', 'donnees', parametres.donnees, 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 0])
					.SADD('interactions-creees:' + identifiant, code.toString())
					.exec()
					if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + interaction))) {
						await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + interaction), path.join(__dirname, '..', '/static/fichiers/' + code))
						res.json({ type: parametres.type, titre: t[req.session.langue].copieDe + parametres.titre, code: code, identifiant: identifiant, motdepasse: '', donnees: JSON.parse(parametres.donnees), reponses: {}, sessions: {}, statut: '', session: 1, date: date })
					} else {
						res.json({ type: parametres.type, titre: t[req.session.langue].copieDe + parametres.titre, code: code, identifiant: identifiant, motdepasse: '', donnees: JSON.parse(parametres.donnees), reponses: {}, sessions: {}, statut: '', session: 1, date: date })
					}
				} else {
					res.send('existe_deja')
				}
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/exporter-interaction', async function (req, res) {
		const identifiant = req.body.identifiant
		const admin = req.body.admin
		const motdepasseAdmin = process.env.VITE_ADMIN_PASSWORD
		if ((req.session.identifiant && req.session.identifiant === identifiant) || (admin !== '' && admin === motdepasseAdmin)) {
			const code = parseInt(req.body.code)
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				let parametres = await db.HGETALL('interactions:' + code)
				parametres = Object.assign({}, parametres)
				if (parametres === null) { res.send('erreur'); return false }
				if (admin === '' && ((parametres.hasOwnProperty('motdepasse') && req.body.hasOwnProperty('motdepasse') && parametres.motdepasse !== req.body.motdepasse) || (parametres.hasOwnProperty('identifiant') && parametres.identifiant !== identifiant))) {
					res.send('non_autorise')
					return false
				}
				const chemin = path.join(__dirname, '..', '/static/temp')
				await fs.mkdirp(path.normalize(chemin + '/' + code))
				await fs.mkdirp(path.normalize(chemin + '/' + code + '/fichiers'))
				await fs.writeFile(path.normalize(chemin + '/' + code + '/donnees.json'), JSON.stringify(parametres, '', 4), 'utf8')
				const donnees = JSON.parse(parametres.donnees)
				if (Object.keys(donnees).length > 0) {
					const fichiers = []
					if (Object.keys(donnees.support).length > 0) {
						if (donnees.support.hasOwnProperty('fichier')) {
							fichiers.push(donnees.support.fichier)
						} else if (donnees.support.hasOwnProperty('image')) {
							fichiers.push(donnees.support.image)
						}
					}
					if (parametres.type === 'Sondage' || parametres.type === 'Questionnaire') {
						if (donnees.hasOwnProperty('questions')) {
							donnees.questions.forEach(function (q) {
								if (Object.keys(q.support).length > 0) {
									if (q.support.hasOwnProperty('fichier')) {
										fichiers.push(q.support.fichier)
									} else if (q.support.hasOwnProperty('image')) {
										fichiers.push(q.support.image)
									}
								}
								q.items.forEach(function (item) {
									if (item.image !== '') {
										fichiers.push(item.image)
									}
								})
							})
						} else {
							donnees.items.forEach(function (item) {
								if (item.image !== '') {
									fichiers.push(item.image)
								}
							})
						}
					} else if (parametres.type === 'Remue-méninges') {
						donnees.categories.forEach(function (categorie) {
							if (categorie.image !== '') {
								fichiers.push(categorie.image)
							}
						})
					}
					for (const fichier of fichiers) {
						if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier))) {
							await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier), path.normalize(chemin + '/' + code + '/fichiers/' + fichier, { overwrite: true }))
						}
					}
					const archiveId = Math.floor((Math.random() * 100000) + 1)
					const sortie = fs.createWriteStream(path.normalize(chemin + '/' + code + '_' + archiveId + '.zip'))
					const archive = archiver('zip', {
						zlib: { level: 9 }
					})
					sortie.on('finish', async function () {
						await fs.remove(path.normalize(chemin + '/' + code))
						res.send(code + '_' + archiveId + '.zip')
					})
					archive.pipe(sortie)
					archive.directory(path.normalize(chemin + '/' + code), false)
					archive.finalize()
				} else {
					res.send('erreur_donnees')
				}
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/importer-interaction', function (req, res) {
		const identifiant = req.session.identifiant
		if (!identifiant) {
			res.send('non_autorise')
		} else {
			televerserArchive(req, res, async function (err) {
				if (err) { res.send('erreur_import'); return false }
				try {
					const source = path.join(__dirname, '..', '/static/temp/' + req.file.filename)
					const cible = path.join(__dirname, '..', '/static/temp/archive-' + Math.floor((Math.random() * 100000) + 1))
					await extract(source, { dir: cible })
					const donnees = await fs.readJson(path.normalize(cible + '/donnees.json'))
					const parametres = JSON.parse(req.body.parametres)
					// Vérification des clés des données
					if (donnees.hasOwnProperty('type') && donnees.hasOwnProperty('titre') && donnees.hasOwnProperty('code') && donnees.hasOwnProperty('motdepasse') && donnees.hasOwnProperty('donnees') && donnees.hasOwnProperty('reponses') && donnees.hasOwnProperty('sessions') && donnees.hasOwnProperty('statut') && donnees.hasOwnProperty('session') && donnees.hasOwnProperty('date')) {
						const code = Math.floor(1000000 + Math.random() * 9000000)
						const date = dayjs().format()
						const reponse = await db.EXISTS('interactions:' + code)
						if (reponse === null) { res.send('erreur'); return false }
						if (reponse === 0) {
							if (parametres.resultats === true) {
								await db.HSET('interactions:' + code, ['type', donnees.type, 'titre', donnees.titre, 'code', code, 'identifiant', identifiant, 'motdepasse', '', 'donnees', donnees.donnees, 'reponses', donnees.reponses, 'sessions', donnees.sessions, 'statut', '', 'session', donnees.session, 'date', date, 'digidrive', 0])
							} else {
								await db.HSET('interactions:' + code, ['type', donnees.type, 'titre', donnees.titre, 'code', code, 'identifiant', identifiant, 'motdepasse', '', 'donnees', donnees.donnees, 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 0])
							}
							await db.SADD('interactions-creees:' + identifiant, code.toString())
							const chemin = path.join(__dirname, '..', '/static/fichiers/' + code)
							await fs.move(path.normalize(cible + '/fichiers'), chemin)
							if (parametres.resultats === true) {
								res.json({ type: donnees.type, titre: donnees.titre, code: code, identifiant: identifiant, motdepasse: '', donnees: JSON.parse(donnees.donnees), reponses: JSON.parse(donnees.reponses), sessions: JSON.parse(donnees.sessions), statut: '', session: donnees.session, date: date })
							} else {
								res.json({ type: donnees.type, titre: donnees.titre, code: code, identifiant: identifiant, motdepasse: '', donnees: JSON.parse(donnees.donnees), reponses: {}, sessions: {}, statut: '', session: 1, date: date })
							}
						} else {
							res.send('existe_deja')
						}
					} else {
						await fs.remove(source)
						await fs.remove(cible)
						res.send('donnees_corrompues')
					}
				} catch (err) {
					await fs.remove(path.join(__dirname, '..', '/static/temp/' + req.file.filename))
					res.send('erreur_import')
				}
			})
		}
	})

	app.post('/api/supprimer-interaction', async function (req, res) {
		const code = parseInt(req.body.code)
		const identifiant = req.body.identifiant
		const admin = req.body.admin
		const motdepasseAdmin = process.env.VITE_ADMIN_PASSWORD
		if ((req.session.identifiant && req.session.identifiant === identifiant) || (admin !== '' && admin === motdepasseAdmin)) {
			let suppressionFichiers = true
			if (req.body.hasOwnProperty('suppressionFichiers')) {
				suppressionFichiers = req.body.suppressionFichiers
			}
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { res.send('erreur'); return false }
				if (admin === '' && ((resultat.hasOwnProperty('motdepasse') && req.body.hasOwnProperty('motdepasse') && resultat.motdepasse !== req.body.motdepasse) || (resultat.hasOwnProperty('identifiant') && resultat.identifiant !== identifiant))) {
					res.send('non_autorise')
					return false
				}
				await db
				.multi()
				.DEL('interactions:' + code)
				.SREM('interactions-creees:' + identifiant, code.toString())
				.exec()
				if (suppressionFichiers === true) {
					await fs.remove(path.join(__dirname, '..', '/static/fichiers/' + code))
				}
				res.send('interaction_supprimee')
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/exporter-resultat', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const code = parseInt(req.body.code)
			let resultat = await db.HGETALL('interactions:' + code)
			resultat = Object.assign({}, resultat)
			if (resultat === null) { res.send('erreur'); return false }
			if ((resultat.hasOwnProperty('motdepasse') && resultat.motdepasse !== req.body.motdepasse) || (resultat.hasOwnProperty('identifiant') && resultat.identifiant !== identifiant)) {
				res.send('non_autorise')
				return false
			}
			const type = req.body.type
			const titre = req.body.titre
			const donnees = req.body.donnees
			const reponses = req.body.reponses
			const dateDebut = req.body.dateDebut
			const dateFin = req.body.dateFin
			const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
			const doc = new PDFDocument()
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/resultats.pdf')
			const flux = fs.createWriteStream(chemin)
			doc.pipe(flux)
			doc.fontSize(16)
			if (type === 'Sondage') {
				doc.font('Helvetica-Bold').text(t[req.session.langue].sondage + ' - ' + titre)
			} else if (type === 'Questionnaire') {
				doc.font('Helvetica-Bold').text(t[req.session.langue].questionnaire + ' - ' + titre)
			} else if (type === 'Remue-méninges') {
				doc.font('Helvetica-Bold').text(t[req.session.langue].remueMeninges + ' - ' + titre)
			} else if (type === 'Nuage-de-mots') {
				doc.font('Helvetica-Bold').text(t[req.session.langue].nuageDeMots + ' - ' + titre)
			}
			doc.fontSize(10)
			doc.moveDown()
			if (type === 'Sondage' && donnees !== null && donnees.hasOwnProperty('question')) {
				if (dateDebut !== '' && dateFin !== '') {
					doc.fontSize(8)
					doc.font('Helvetica').text(formaterDate(dateDebut, t[req.session.langue].demarre, req.session.langue) + ' - ' + formaterDate(dateFin, t[req.session.langue].termine, req.session.langue))
					doc.moveDown()
				}
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].question, { underline: true })
				if (donnees.question !== '') {
					doc.moveDown()
					doc.font('Helvetica-Bold').text(donnees.question)
				}
				if (Object.keys(donnees.support).length > 0) {
					doc.moveDown()
					const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.image)
					if (await fs.pathExists(cheminSupport)) {
						const support = await fs.readFile(cheminSupport)
						if (support && magic.includes(support.toString('hex', 0, 4)) === true) {
							doc.image(support, { fit: [120, 120] })
						} else {
							doc.fontSize(10)
							doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
						}
					} else {
						doc.fontSize(10)
						doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
					}
				}
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + reponses.length + ')', { underline: true })
				doc.moveDown()
				let items = donnees.items
				if (donnees.option === 'texte-court') {
					items = []
					reponses.forEach(function (donnees) {
						donnees.reponse.forEach(function (reponse) {
							if (!items.includes(reponse.toString().trim())) {
								items.push(reponse.toString().trim())
							}
						})
					})
				}
				const statistiques = definirStatistiquesSondage(donnees, reponses)
				for (let i = 0; i < items.length; i++) {
					if (donnees.option !== 'texte-court' && items[i].texte !== '') {
						doc.fontSize(10)
						doc.font('Helvetica').text(alphabet[i] + '. ' + items[i].texte + ' (' + statistiques.pourcentages[i] + '% - ' + statistiques.personnes[i] + ')')
						if (items[i].image !== '') {
							const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + items[i].image)
							if (await fs.pathExists(cheminImage)) {
								const image = await fs.readFile(cheminImage)
								if (image && magic.includes(image.toString('hex', 0, 4)) === true) {
									doc.image(image, { fit: [75, 75] })
								}
							}
						}
					} else if (donnees.option !== 'texte-court' && items[i].image !== '') {
						doc.fontSize(10)
						const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + items[i].image)
						if (await fs.pathExists(cheminImage)) {
							const image = await fs.readFile(cheminImage)
							doc.font('Helvetica').text(alphabet[i] + '. (' + statistiques.pourcentages[i] + '% - ' + statistiques.personnes[i] + ')').image(image, { fit: [75, 75] })
						} else {
							doc.font('Helvetica').text(alphabet[i] + '. ' + items[i].alt + ' (' + statistiques.pourcentages[i] + '% - ' + statistiques.personnes[i] + ')')
						}
					} else {
						doc.fontSize(10)
						doc.font('Helvetica').text((i + 1) + '. ' + items[i] + ' (' + statistiques.pourcentages[i] + '% - ' + statistiques.personnes[i] + ')')
					}
					doc.moveDown()
				}
			} else if (type === 'Sondage' && donnees !== null && donnees.hasOwnProperty('questions')) {
				const statistiques = definirStatistiquesQuestions(donnees.questions, reponses)
				if (dateDebut !== '' && dateFin !== '') {
					doc.fontSize(8)
					doc.font('Helvetica').text(formaterDate(dateDebut, t[req.session.langue].demarre, req.session.langue) + ' - ' + formaterDate(dateFin, t[req.session.langue].termine, req.session.langue))
					doc.moveDown()
				}
				if (donnees.options.progression === 'libre') {
					doc.font('Helvetica').text(t[req.session.langue].progression + ' ' + t[req.session.langue].progressionLibre)
				} else {
					doc.font('Helvetica').text(t[req.session.langue].progression + ' ' + t[req.session.langue].progressionAnimateur)
				}
				doc.moveDown()
				doc.moveDown()
				if (donnees.description !== '') {
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].description, { underline: true })
					doc.fontSize(10)
					doc.moveDown()
					doc.font('Helvetica').text(donnees.description)
				}
				if (donnees.description !== '' && Object.keys(donnees.support).length > 0) {
					doc.fontSize(12)
					doc.moveDown()
				}
				if (Object.keys(donnees.support).length > 0) {
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].support, { underline: true })
					doc.fontSize(10)
					doc.moveDown()
					if (donnees.support.type === 'image' && donnees.support.fichier !== '') {
						const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.fichier)
						if (await fs.pathExists(cheminSupport)) {
							const support = await fs.readFile(cheminSupport)
							if (support && magic.includes(support.toString('hex', 0, 4)) === true) {
								doc.image(support, { fit: [120, 120] })
							} else {
								doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
							}
						} else {
							doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
						}
					} else if (donnees.support.type === 'audio') {
						doc.font('Helvetica').text(t[req.session.langue].fichierAudio + ' ' + donnees.support.alt)
					} else if (donnees.support.type === 'video') {
						doc.font('Helvetica').text(t[req.session.langue].video, {
							link: donnees.support.lien,
							underline: true
						})
					}
				}
				if (donnees.description !== '' || Object.keys(donnees.support).length > 0) {
					doc.fontSize(12)
					doc.moveDown()
					doc.moveDown()
				}
				for (let i = 0; i < donnees.questions.length; i++) {
					doc.fontSize(14)
					doc.font('Helvetica-Bold').fillColor('black').text(t[req.session.langue].question + ' ' + (i + 1))
					doc.fontSize(10)
					doc.font('Helvetica').text('-----------------------------------------------')
					doc.fontSize(14)
					doc.moveDown()
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].question, { underline: true })
					if (donnees.questions[i].question !== '') {
						doc.moveDown()
						doc.font('Helvetica-Bold').text(donnees.questions[i].question)
					}
					if (Object.keys(donnees.questions[i].support).length > 0) {
						doc.moveDown()
						const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.questions[i].support.image)
						if (await fs.pathExists(cheminSupport)) {
							const support = await fs.readFile(cheminSupport)
							if (support && magic.includes(support.toString('hex', 0, 4)) === true) {
								doc.image(support, { fit: [120, 120] })
							}
						}
					}
					doc.moveDown()
					doc.moveDown()
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + definirReponses(reponses, i) + ')', { underline: true })
					doc.moveDown()
					if (donnees.questions[i].option !== 'texte-court') {
						for (let j = 0; j < donnees.questions[i].items.length; j++) {
							if (donnees.questions[i].items[j].texte !== '') {
								doc.fontSize(10)
								doc.font('Helvetica').text(alphabet[j] + '. ' + donnees.questions[i].items[j].texte + ' (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ')')
								if (donnees.questions[i].items[j].image !== '') {
									const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.questions[i].items[j].image)
									if (await fs.pathExists(cheminImage)) {
										const image = await fs.readFile(cheminImage)
										if (image && magic.includes(image.toString('hex', 0, 4)) === true) {
											doc.image(image, { fit: [75, 75] })
										}
									}
								}
							} else if (donnees.questions[i].items[j].image !== '') {
								doc.fontSize(10)
								const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.questions[i].items[j].image)
								if (await fs.pathExists(cheminImage)) {
									const image = await fs.readFile(cheminImage)
									doc.font('Helvetica').text(alphabet[j] + '. (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ')').image(image, { fit: [75, 75] })
								} else {
									doc.font('Helvetica').text(alphabet[j] + '. ' + donnees.questions[i].items[j].alt + ' (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ')')
								}
							}
							doc.moveDown()
						}
					} else {
						const itemsTexte = []
						reponses.forEach(function (donnees) {
							donnees.reponse[i].forEach(function (reponse) {
								if (!itemsTexte.includes(reponse.toString().trim())) {
									itemsTexte.push(reponse.toString().trim())
								}
							})
						})
						itemsTexte.forEach(async function (item, index) {
							doc.fontSize(10)
							doc.font('Helvetica').text((index + 1) + '. ' + item + ' (' + statistiques[i].pourcentages[index] + '% - ' + statistiques[i].personnes[index] + ')')
							doc.moveDown()
						})
					}
					doc.moveDown()
					doc.moveDown()
				}
			} else if (type === 'Questionnaire' && donnees !== null) {
				const statistiques = definirStatistiquesQuestions(donnees.questions, reponses)
				const classement = req.body.classement
				if (dateDebut !== '' && dateFin !== '') {
					doc.fontSize(8)
					doc.font('Helvetica').text(formaterDate(dateDebut, t[req.session.langue].demarre, req.session.langue) + ' - ' + formaterDate(dateFin, t[req.session.langue].termine, req.session.langue))
					doc.moveDown()
				}
				if (donnees.options.progression === 'libre') {
					doc.font('Helvetica').text(t[req.session.langue].progression + ' ' + t[req.session.langue].progressionLibre)
				} else {
					doc.font('Helvetica').text(t[req.session.langue].progression + ' ' + t[req.session.langue].progressionAnimateur)
				}
				doc.moveDown()
				doc.moveDown()
				if (donnees.description !== '') {
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].description, { underline: true })
					doc.fontSize(10)
					doc.moveDown()
					doc.font('Helvetica').text(donnees.description)
				}
				if (donnees.description !== '' && Object.keys(donnees.support).length > 0) {
					doc.fontSize(12)
					doc.moveDown()
				}
				if (Object.keys(donnees.support).length > 0) {
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].support, { underline: true })
					doc.fontSize(10)
					doc.moveDown()
					if (donnees.support.type === 'image') {
						const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.fichier)
						if (await fs.pathExists(cheminSupport)) {
							const support = await fs.readFile(cheminSupport)
							if (support && magic.includes(support.toString('hex', 0, 4)) === true) {
								doc.image(support, { fit: [120, 120] })
							} else {
								doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
							}
						} else {
							doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
						}
					} else if (donnees.support.type === 'audio') {
						doc.font('Helvetica').text(t[req.session.langue].fichierAudio + ' ' + donnees.support.alt)
					} else if (donnees.support.type === 'video') {
						doc.font('Helvetica').text(t[req.session.langue].video, {
							link: donnees.support.lien,
							underline: true
						})
					}
				}
				if (donnees.description !== '' || Object.keys(donnees.support).length > 0) {
					doc.fontSize(12)
					doc.moveDown()
					doc.moveDown()
				}
				for (let i = 0; i < donnees.questions.length; i++) {
					doc.fontSize(14)
					doc.font('Helvetica-Bold').fillColor('black').text(t[req.session.langue].question + ' ' + (i + 1))
					doc.fontSize(10)
					doc.font('Helvetica').text('-----------------------------------------------')
					doc.fontSize(14)
					doc.moveDown()
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].question, { underline: true })
					if (donnees.questions[i].question !== '') {
						doc.moveDown()
						doc.font('Helvetica-Bold').text(donnees.questions[i].question)
					}
					if (Object.keys(donnees.questions[i].support).length > 0) {
						doc.moveDown()
						const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.questions[i].support.image)
						if (await fs.pathExists(cheminSupport)) {
							const support = await fs.readFile(cheminSupport)
							if (support && magic.includes(support.toString('hex', 0, 4)) === true) {
								doc.image(support, { fit: [120, 120] })
							}
						}
					}
					doc.moveDown()
					doc.moveDown()
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + definirReponses(reponses, i) + ')', { underline: true })
					doc.moveDown()
					if (donnees.questions[i].option !== 'texte-court') {
						for (let j = 0; j < donnees.questions[i].items.length; j++) {
							if (donnees.questions[i].items[j].texte !== '') {
								doc.fontSize(10)
								if (donnees.questions[i].items[j].reponse === true) {
									doc.font('Helvetica').fillColor('#00a695').text(alphabet[j] + '. ' + donnees.questions[i].items[j].texte + ' (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ') - ' + t[req.session.langue].bonneReponse)
								} else {
									doc.font('Helvetica').fillColor('grey').text(alphabet[j] + '. ' + donnees.questions[i].items[j].texte + ' (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ')')
								}
								if (donnees.questions[i].items[j].image !== '') {
									const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.questions[i].items[j].image)
									if (await fs.pathExists(cheminImage)) {
										const image = await fs.readFile(cheminImage)
										if (image && magic.includes(image.toString('hex', 0, 4)) === true) {
											doc.image(image, { fit: [75, 75] })
										}
									}
								}
							} else if (donnees.questions[i].items[j].image !== '') {
								doc.fontSize(10)
								const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.questions[i].items[j].image)
								if (await fs.pathExists(cheminImage)) {
									const image = await fs.readFile(cheminImage)
									if (donnees.questions[i].items[j].reponse === true) {
										doc.font('Helvetica').fillColor('#00a695').text(alphabet[j] + '. (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ') - ' + t[req.session.langue].bonneReponse).image(image, { fit: [75, 75] })
									} else {
										doc.font('Helvetica').fillColor('grey').text(alphabet[j] + '. (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ')').image(image, { fit: [75, 75] })
									}
								} else if (!await fs.pathExists(cheminImage) && donnees.questions[i].items[j].reponse === true) {
									doc.font('Helvetica').fillColor('#00a695').text(alphabet[j] + '. ' + donnees.questions[i].items[j].alt + ' (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ') - ' + t[req.session.langue].bonneReponse)
								} else if (!await fs.pathExists(cheminImage) && donnees.questions[i].items[j].reponse === false) {
									doc.font('Helvetica').fillColor('grey').text(alphabet[j] + '. ' + donnees.questions[i].items[j].alt + ' (' + statistiques[i].pourcentages[j] + '% - ' + statistiques[i].personnes[j] + ')')
								}
							}
							doc.moveDown()
						}
					} else {
						const itemsTexte = []
						reponses.forEach(function (donnees) {
							donnees.reponse[i].forEach(function (reponse) {
								if (!itemsTexte.includes(reponse.toString().trim())) {
									itemsTexte.push(reponse.toString().trim())
								}
							})
						})
						const reponsesTexte = donnees.questions[i].reponses.split(',')
						reponsesTexte.forEach(function (item, index) {
							reponsesTexte[index] = item.trim()
						})
						itemsTexte.forEach(async function (item, index) {
							doc.fontSize(10)
							if (reponsesTexte.includes(item) === true) {
								doc.font('Helvetica').fillColor('#00a695').text((index + 1) + '. ' + item + ' (' + statistiques[i].pourcentages[index] + '% - ' + statistiques[i].personnes[index] + ') - ' + t[req.session.langue].bonneReponse)
							} else {
								doc.font('Helvetica').fillColor('grey').text((index + 1) + '. ' + item + ' (' + statistiques[i].pourcentages[index] + '% - ' + statistiques[i].personnes[index] + ')')
							}
							doc.moveDown()
						})
					}
					doc.moveDown()
					doc.moveDown()
				}
				if (classement.length > 0 && donnees.hasOwnProperty('options') && donnees.options.nom === 'obligatoire') {
					doc.fontSize(14)
					doc.font('Helvetica-Bold').fillColor('black').text(t[req.session.langue].classement)
					doc.fontSize(10)
					doc.font('Helvetica').text('-----------------------------------------------')
					doc.fontSize(14)
					doc.moveDown()
					doc.fontSize(12)
					classement.forEach(function (utilisateur, indexUtilisateur) {
						doc.font('Helvetica').text((indexUtilisateur + 1) + '. ' + utilisateur.nom + ' (' + (Math.round(utilisateur.score * 10) / 10) + ' ' + t[req.session.langue].points + ')')
						doc.moveDown()
					})
				}
			} else if (type === 'Remue-méninges' && donnees !== null) {
				let categories = []
				if (donnees.hasOwnProperty('categories')) {
					categories = donnees.categories.filter(function (categorie) {
						return categorie.texte !== '' || categorie.image !== ''
					})
				}
				const messages = definirMessagesRemueMeninges(categories, reponses)
				if (dateDebut !== '' && dateFin !== '') {
					doc.fontSize(8)
					doc.font('Helvetica').text(formaterDate(dateDebut, t[req.session.langue].demarre, req.session.langue) + ' - ' + formaterDate(dateFin, t[req.session.langue].termine, req.session.langue))
					doc.moveDown()
				}
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].question, { underline: true })
				doc.moveDown()
				doc.font('Helvetica-Bold').text(donnees.question)
				if (Object.keys(donnees.support).length > 0) {
					doc.fontSize(10)
					doc.moveDown()
					doc.moveDown()
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].support, { underline: true })
					doc.fontSize(10)
					doc.moveDown()
					if (donnees.support.type === 'image' && donnees.support.fichier !== '') {
						const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.fichier)
						if (await fs.pathExists(cheminSupport)) {
							const support = await fs.readFile(cheminSupport)
							if (support && magic.includes(support.toString('hex', 0, 4)) === true) {
								doc.image(support, { fit: [120, 120] })
								doc.moveDown()
							} else {
								doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
								doc.moveDown()
							}
						} else {
							doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
							doc.moveDown()
						}
					} else if (donnees.support.type === 'audio') {
						doc.font('Helvetica').text(t[req.session.langue].fichierAudio + ' ' + donnees.support.alt)
						doc.moveDown()
					} else if (donnees.support.type === 'video') {
						doc.font('Helvetica').text(t[req.session.langue].video, {
							link: donnees.support.lien,
							underline: true
						})
						doc.moveDown()
					}
				}
				doc.moveDown()
				doc.fontSize(12)
				// Messages visibles
				if (categories.length > 0) {
					let totalMessagesVisibles = 0
					messages.visibles.forEach(function (categorie) {
						totalMessagesVisibles = totalMessagesVisibles + categorie.length
					})
					doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + totalMessagesVisibles + ')', { underline: true })
					doc.moveDown()
					for (let i = 0; i < categories.length; i++) {
						if (categories[i].texte !== '') {
							doc.fontSize(10)
							doc.font('Helvetica-Bold').text((i + 1) + '. ' + categories[i].texte + ' (' + messages.visibles[i].length + ')')
							if (categories[i].image !== '') {
								const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + categories[i].image)
								if (await fs.pathExists(cheminImage)) {
									const image = await fs.readFile(cheminImage)
									if (image && magic.includes(image.toString('hex', 0, 4)) === true) {
										doc.image(image, { fit: [40, 40] })
										doc.moveDown()
									}
								}
							}
							messages.visibles[i].forEach(function (message) {
								doc.fontSize(9)
								doc.font('Helvetica').text('• ' + message.reponse.texte)
							})
						} else if (categories[i].image !== '') {
							doc.fontSize(10)
							const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + categories[i].image)
							if (await fs.pathExists(cheminImage)) {
								const image = await fs.readFile(cheminImage)
								if (image) {
									doc.font('Helvetica-Bold').text((i + 1) + '. (' + messages.visibles[i].length + ')').image(image, { fit: [40, 40] })
									doc.moveDown()
								} else {
									doc.font('Helvetica-Bold').text((i + 1) + '. ' + categories[i].alt + ' (' + messages.visibles[i].length + ')')
								}
							} else {
								doc.font('Helvetica-Bold').text((index + 1) + '. ' + categories[i].alt + ' (' + messages.visibles[i].length + ')')
							}
							messages.visibles[i].forEach(function (message) {
								doc.fontSize(9)
								doc.font('Helvetica').text('• ' + message.reponse.texte)
							})
						}
						doc.moveDown()
					}
				} else {
					doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + messages.visibles.length + ')', { underline: true })
					doc.moveDown()
					messages.visibles.forEach(function (message) {
						doc.fontSize(9)
						doc.font('Helvetica').text('• ' + message.reponse.texte)
					})
				}
				// Messages supprimés
				if (messages.supprimes.length > 0) {
					doc.moveDown()
					doc.fontSize(12)
					if (categories.length > 0) {
						let totalMessagesSupprimes = 0
						messages.supprimes.forEach(function (categorie) {
							totalMessagesSupprimes = totalMessagesSupprimes + categorie.length
						})
						doc.font('Helvetica-Bold').text(t[req.session.langue].messagesSupprimes + ' (' + totalMessagesSupprimes + ')', { underline: true })
						doc.moveDown()
						for (let i = 0; i < categories.length; i++) {
							if (categories[i].texte !== '') {
								doc.fontSize(10)
								doc.font('Helvetica-Bold').text((i + 1) + '. ' + categories[i].texte + ' (' + messages.supprimes[i].length + ')')
								if (categories[i].image !== '') {
									const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + categories[i].image)
									if (await fs.pathExists(cheminImage)) {
										const image = await fs.readFile(cheminImage)
										if (image && magic.includes(image.toString('hex', 0, 4)) === true) {
											doc.image(image, { fit: [40, 40] })
											doc.moveDown()
										}
									}
								}
								messages.supprimes[i].forEach(function (message) {
									doc.fontSize(9)
									doc.font('Helvetica').text('• ' + message.reponse.texte)
								})
							} else if (categories[i].image !== '') {
								doc.fontSize(10)
								const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + categories[i].image)
								if (await fs.pathExists(cheminImage)) {
									const image = await fs.readFile(cheminImage)
									doc.font('Helvetica-Bold').text((i + 1) + '. (' + messages.supprimes[i].length + ')').image(image, { fit: [40, 40] })
									doc.moveDown()
								} else {
									doc.font('Helvetica-Bold').text((index + 1) + '. ' + categories[i].alt + ' (' + messages.supprimes[i].length + ')')
								}
								messages.supprimes[i].forEach(function (message) {
									doc.fontSize(9)
									doc.font('Helvetica').text('• ' + message.reponse.texte)
								})
							}
							doc.moveDown()
						}
					} else {
						doc.font('Helvetica-Bold').text(t[req.session.langue].messagesSupprimes + ' (' + messages.supprimes.length + ')', { underline: true })
						doc.moveDown()
						messages.supprimes.forEach(function (message) {
							doc.fontSize(9)
							doc.font('Helvetica').text('• ' + message.reponse.texte)
						})
					}
				}
			} else if (type === 'Nuage-de-mots' && donnees !== null) {
				const mots = definirMotsNuageDeMots(reponses)
				if (dateDebut !== '' && dateFin !== '') {
					doc.fontSize(8)
					doc.font('Helvetica').text(formaterDate(dateDebut, t[req.session.langue].demarre, req.session.langue) + ' - ' + formaterDate(dateFin, t[req.session.langue].termine, req.session.langue))
					doc.moveDown()
				}
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].question, { underline: true })
				doc.moveDown()
				doc.font('Helvetica-Bold').text(donnees.question)
				if (Object.keys(donnees.support).length > 0) {
					doc.fontSize(10)
					doc.moveDown()
					doc.moveDown()
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].support, { underline: true })
					doc.fontSize(10)
					doc.moveDown()
					if (donnees.support.type === 'image' && donnees.support.fichier !== '') {
						const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.fichier)
						if (await fs.pathExists(cheminSupport)) {
							const support = await fs.readFile(cheminSupport)
							if (support && magic.includes(support.toString('hex', 0, 4)) === true) {
								doc.image(support, { fit: [120, 120] })
								doc.moveDown()
							} else {
								doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
								doc.moveDown()
							}
						} else {
							doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
							doc.moveDown()
						}
					} else if (donnees.support.type === 'audio') {
						doc.font('Helvetica').text(t[req.session.langue].fichierAudio + ' ' + donnees.support.alt)
						doc.moveDown()
					} else if (donnees.support.type === 'video') {
						doc.font('Helvetica').text(t[req.session.langue].video, {
							link: donnees.support.lien,
							underline: true
						})
						doc.moveDown()
					}
				}
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + mots.visibles.length + ')', { underline: true })
				doc.moveDown()
				mots.visibles.forEach(function (mot) {
					doc.fontSize(9)
					doc.font('Helvetica').text('• ' + mot.reponse.texte)
				})
				if (mots.supprimes.length > 0) {
					doc.moveDown()
					doc.fontSize(12)
					doc.font('Helvetica-Bold').text(t[req.session.langue].motsSupprimes + ' (' + mots.supprimes.length + ')', { underline: true })
					doc.moveDown()
					mots.supprimes.forEach(function (mot) {
						doc.fontSize(9)
						doc.font('Helvetica').text('• ' + mot.reponse.texte)
					})
				}
			}
			doc.end()
			flux.on('finish', function () {
				res.send('resultat_exporte')
			})
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/supprimer-resultat', async function (req, res) {
		const identifiant = req.body.identifiant
		if (req.session.identifiant && req.session.identifiant === identifiant) {
			const code = parseInt(req.body.code)
			const session = parseInt(req.body.session)
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { res.send('erreur'); return false }
			if (reponse === 1) {
				let donnees = await db.HGETALL('interactions:' + code)
				donnees = Object.assign({}, donnees)
				if (donnees === null) { res.send('erreur'); return false }
				if ((donnees.hasOwnProperty('motdepasse') && donnees.motdepasse !== req.body.motdepasse) || (donnees.hasOwnProperty('identifiant') && donnees.identifiant !== identifiant)) {
					res.send('non_autorise')
					return false
				}
				const reponses = JSON.parse(donnees.reponses)
				const sessions = JSON.parse(donnees.sessions)
				if (reponses[session]) {
					delete reponses[session]
				}
				if (sessions[session]) {
					delete sessions[session]
				}
				await db.HSET('interactions:' + code, ['reponses', JSON.stringify(reponses), 'sessions', JSON.stringify(sessions)])
				res.json({ reponses: reponses, sessions: sessions })
			} else {
				res.send('erreur_code')
			}
		} else {
			res.send('non_autorise')
		}
	})

	app.post('/api/televerser-image', function (req, res) {
		const identifiant = req.session.identifiant
		if (!identifiant) {
			res.send('non_autorise')
		} else {
			televerser(req, res, async function (err) {
				if (err) { res.send('erreur'); return false }
				const fichier = req.file
				if (fichier.hasOwnProperty('filename')) {
					let alt = path.parse(fichier.filename).name
					if (fichier.hasOwnProperty('originalname')) {
						alt = path.parse(fichier.originalname).name
					}
					const code = req.body.code
					const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier.filename)
					const extension = path.parse(fichier.filename).ext
					if (extension.toLowerCase() === '.jpg' || extension.toLowerCase() === '.jpeg') {
						try {
							const buffer = await sharp(chemin, { failOnError: false }).withMetadata().rotate().jpeg().resize(1000, 1000, {
								fit: sharp.fit.inside,
								withoutEnlargement: true
							}).toBuffer()
							if (buffer !== null) {
								await fs.writeFile(chemin, buffer)
								res.json({ image: fichier.filename, alt: alt })
							}  else {
								res.send('erreur')
							}
						} catch (e) {
							res.send('erreur')
						}
					} else {
						try {
							const buffer = await sharp(chemin, { failOnError: false }).withMetadata().resize(1000, 1000, {
								fit: sharp.fit.inside,
								withoutEnlargement: true
							}).toBuffer()
							if (buffer !== null) {
								await fs.writeFile(chemin, buffer)
								res.json({ image: fichier.filename, alt: alt })
							} else {
								res.send('erreur')
							}
						} catch (e) {
							res.send('erreur')
						}
					}
				} else {
					res.send('erreur')
				}
			})
		}
	})

	app.post('/api/dupliquer-images', function (req, res) {
		const code = req.body.code
		const images = req.body.images
		images.forEach(async function (image) {
			if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + image))) {
				await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + image), path.join(__dirname, '..', '/static/fichiers/' + code + '/dup-' + image))
			}
		})
		res.send('images_dupliquees')
	})

	app.post('/api/televerser-media', function (req, res) {
		const identifiant = req.session.identifiant
		if (!identifiant) {
			res.send('non_autorise')
		} else {
			televerser(req, res, async function (err) {
				if (err) { res.send('erreur'); return false }
				const fichier = req.file
				if (fichier.hasOwnProperty('filename') && fichier.hasOwnProperty('originalname')) {
					const info = path.parse(fichier.originalname)
					const alt = info.name
					const extension = info.ext.toLowerCase()
					const code = req.body.code
					const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier.filename)
					if (extension === '.jpg' || extension === '.jpeg') {
						try {
							const buffer = await sharp(chemin, { failOnError: false }).withMetadata().rotate().jpeg().resize(1000, 1000, {
								fit: sharp.fit.inside,
								withoutEnlargement: true
							}).toBuffer()
							if (buffer !== null) {
								await fs.writeFile(chemin, buffer)
								res.json({ fichier: fichier.filename, alt: alt, type: 'image' })
							} else {
								res.send('erreur')
							}
						} catch (e) {
							res.send('erreur')
						}
					} else if (extension === '.png' || extension === '.gif') {
						try {
							const buffer = await sharp(chemin, { failOnError: false }).withMetadata().resize(1000, 1000, {
								fit: sharp.fit.inside,
								withoutEnlargement: true
							}).toBuffer()
							if (buffer !== null) {
								await fs.writeFile(chemin, buffer)
								res.json({ fichier: fichier.filename, alt: alt, type: 'image' })
							} else {
								res.send('erreur')
							}
						} catch (e) {
							res.send('erreur')
						}
					} else {
						res.json({ fichier: fichier.filename, alt: alt, type: 'audio' })
					}
				} else {
					res.send('erreur')
				}
			})
		}
	})

	app.post('/api/supprimer-fichiers', function (req, res) {
		const code = req.body.code
		const fichiers = req.body.fichiers
		fichiers.forEach(function (fichier) {
			supprimerFichier(code, fichier)
		})
		res.send('fichiers_supprimes')
	})

	app.post('/api/ladigitale', function (req, res) {
		const tokenApi = req.body.token
		const domaine = req.headers.host
		const lien = req.body.lien
		const params = new URLSearchParams()
		params.append('token', tokenApi)
		params.append('domaine', domaine)
		axios.post(lien, params).then(async function (reponse) {
			if (reponse.data === 'non_autorise' || reponse.data === 'erreur') {
				res.send('erreur_token')
			} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'creer') {
				const titre = req.body.nom
				const type = req.body.interaction
				const code = Math.floor(1000000 + Math.random() * 9000000)
				const motdepasse = req.body.motdepasse
				const date = dayjs().format()
				const reponse = await db.EXISTS('interactions:' + code)
				if (reponse === null) { res.send('erreur'); return false }
				if (reponse === 0) {
					await db.HSET('interactions:' + code, ['type', type, 'titre', titre, 'code', code, 'motdepasse', motdepasse, 'donnees', JSON.stringify({}), 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 1])
					const chemin = path.join(__dirname, '..', '/static/fichiers/' + code)
					await fs.mkdirp(chemin)
					res.send(code.toString())
				} else {
					res.send('erreur')
				}
			} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'modifier-titre') {
				const code = req.body.id
				const titre = req.body.titre
				const reponse = await db.EXISTS('interactions:' + code)
				if (reponse === null) { res.send('erreur'); return false }
				if (reponse === 1) {
					await db.HSET('interactions:' + code, 'titre', titre)
					res.send('titre_modifie')
				} else {
					res.send('contenu_inexistant')
				}
			} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'modifier') {
				const code = req.body.id
				const titre = req.body.titre
				const ancienmotdepasse = req.body.ancienmotdepasse
				const reponse = await db.EXISTS('interactions:' + code)
				if (reponse === null) { res.send('erreur'); return false }
				if (reponse === 1) {
					let donnees = await db.HGETALL('interactions:' + code)
					donnees = Object.assign({}, donnees)
					if (donnees === null) { res.send('erreur'); return false }
					if (donnees.hasOwnProperty('motdepasse') && ancienmotdepasse === donnees.motdepasse) {
						const motdepasse = req.body.motdepasse
						await db.HSET('interactions:' + code, ['titre', titre, 'motdepasse', motdepasse])
						res.send('contenu_modifie')
					} else {
						res.send('non_autorise')
					}
				} else {
					res.send('contenu_inexistant')
				}
			} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'ajouter') {
				const identifiant = req.body.identifiant
				const motdepasse = req.body.motdepasse
				const code = parseInt(req.body.id)
				const reponse = await db.EXISTS('interactions:' + code)
				if (reponse === null) { res.send('erreur'); return false }
				if (reponse === 1) {
					let donnees = await db.HGETALL('interactions:' + code)
					donnees = Object.assign({}, donnees)
					if (donnees === null) { res.send('erreur'); return false }
					if (donnees.hasOwnProperty('motdepasse') && motdepasse === donnees.motdepasse) {
						res.json({ titre: donnees.titre, identifiant: identifiant })
					} else if (donnees.hasOwnProperty('motdepasse') && donnees.motdepasse === '') {
						const resultat = await db.EXISTS('utilisateurs:' + donnees.identifiant)
						if (resultat === null) { res.send('erreur'); return false }
						if (resultat === 1) {
							let utilisateur = await db.HGETALL('utilisateurs:' + donnees.identifiant)
							utilisateur = Object.assign({}, utilisateur)
							if (utilisateur === null) { res.send('erreur'); return false }
							if (motdepasse.trim() !== '' && utilisateur.hasOwnProperty('motdepasse') && utilisateur.motdepasse.trim() !== '' && await bcrypt.compare(motdepasse, utilisateur.motdepasse)) {
								await db.HSET('interactions:' + code, 'digidrive', 1)
								res.json({ titre: donnees.titre, identifiant: donnees.identifiant })
							} else {
								res.send('non_autorise')
							}
						} else {
							res.send('erreur')
						}
					} else {
						res.send('non_autorise')
					}
				} else {
					res.send('contenu_inexistant')
				}
			} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'dupliquer') {
				const identifiant = req.body.identifiant
				const motdepasse = req.body.motdepasse
				const interaction = parseInt(req.body.id)
				const reponse = await db.EXISTS('interactions:' + interaction)
				if (reponse === null) { res.send('erreur'); return false }
				if (reponse === 1) {
					let parametres = await db.HGETALL('interactions:' + interaction)
					parametres = Object.assign({}, parametres)
					if (parametres === null) { res.send('erreur'); return false }
					if (parametres.hasOwnProperty('motdepasse') && motdepasse === parametres.motdepasse) {
						const code = Math.floor(1000000 + Math.random() * 9000000)
						const nouveaumotdepasse = req.body.nouveaumotdepasse
						const date = dayjs().format()
						let resultat = await db.EXISTS('interactions:' + code)
						if (resultat === null) { res.send('erreur'); return false }
						if (resultat === 0) {
							await db.HSET('interactions:' + code, ['type', parametres.type, 'titre', 'Copie de ' + parametres.titre, 'code', code, 'motdepasse', nouveaumotdepasse, 'donnees', parametres.donnees, 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 1])
							if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + interaction))) {
								await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + interaction), path.join(__dirname, '..', '/static/fichiers/' + code))
								res.send(code.toString())
							} else {
								res.send(code.toString())
							}
						} else {
							res.send('erreur')
						}
					} else if (parametres.hasOwnProperty('motdepasse') && parametres.motdepasse === '') {
						const resultat = await db.EXISTS('utilisateurs:' + identifiant)
						if (resultat === null) { res.send('erreur'); return false }
						if (resultat === 1) {
							let utilisateur = await db.HGETALL('utilisateurs:' + identifiant)
							utilisateur = Object.assign({}, utilisateur)
							if (utilisateur === null) { res.send('erreur'); return false }
							if (motdepasse.trim() !== '' && utilisateur.hasOwnProperty('motdepasse') && utilisateur.motdepasse.trim() !== '' && await bcrypt.compare(motdepasse, utilisateur.motdepasse)) {
								const code = Math.floor(1000000 + Math.random() * 9000000)
								const date = dayjs().format()
								let resultat = await db.EXISTS('interactions:' + code)
								if (resultat === null) { res.send('erreur'); return false }
								if (resultat === 0) {
									await db
									.multi()
									.HSET('interactions:' + code, ['type', parametres.type, 'titre', 'Copie de ' + parametres.titre, 'code', code, 'identifiant', identifiant, 'motdepasse', '', 'donnees', parametres.donnees, 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 1])
									.SADD('interactions-creees:' + identifiant, code.toString())
									.exec()
									if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + interaction))) {
										await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + interaction), path.join(__dirname, '..', '/static/fichiers/' + code))
										res.send(code.toString())
									} else {
										res.send(code.toString())
									}
								} else {
									res.send('erreur')
								}
							} else {
								res.send('non_autorise')
							}
						} else {
							res.send('erreur')
						}
					} else {
						res.send('non_autorise')
					}
				} else {
					res.send('contenu_inexistant')
				}
			} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'exporter') {
				const identifiant = req.body.identifiant
				const motdepasse = req.body.motdepasse
				const code = parseInt(req.body.id)
				const reponse = await db.EXISTS('interactions:' + code)
				if (reponse === null) { res.send('erreur'); return false }
				if (reponse === 1) {
					let parametres = await db.HGETALL('interactions:' + code)
					parametres = Object.assign({}, parametres)
					if (parametres === null) { res.send('erreur'); return false }
					if (parametres.hasOwnProperty('motdepasse') && parametres.motdepasse === motdepasse) {
						const chemin = path.join(__dirname, '..', '/static/temp')
						await fs.mkdirp(path.normalize(chemin + '/' + code))
						await fs.mkdirp(path.normalize(chemin + '/' + code + '/fichiers'))
						await fs.writeFile(path.normalize(chemin + '/' + code + '/donnees.json'), JSON.stringify(parametres, '', 4), 'utf8')
						const donnees = JSON.parse(parametres.donnees)
						if (Object.keys(donnees).length > 0) {
							const fichiers = []
							if (Object.keys(donnees.support).length > 0) {
								if (donnees.support.hasOwnProperty('fichier')) {
									fichiers.push(donnees.support.fichier)
								} else if (donnees.support.hasOwnProperty('image')) {
									fichiers.push(donnees.support.image)
								}
							}
							if (parametres.type === 'Sondage' || parametres.type === 'Questionnaire') {
								if (donnees.hasOwnProperty('questions')) {
									donnees.questions.forEach(function (q) {
										if (Object.keys(q.support).length > 0) {
											if (q.support.hasOwnProperty('fichier')) {
												fichiers.push(q.support.fichier)
											} else if (q.support.hasOwnProperty('image')) {
												fichiers.push(q.support.image)
											}
										}
										q.items.forEach(function (item) {
											if (item.image !== '') {
												fichiers.push(item.image)
											}
										})
									})
								} else {
									donnees.items.forEach(function (item) {
										if (item.image !== '') {
											fichiers.push(item.image)
										}
									})
								}
							} else if (parametres.type === 'Remue-méninges') {
								donnees.categories.forEach(function (categorie) {
									if (categorie.image !== '') {
										fichiers.push(categorie.image)
									}
								})
							}
							for (const fichier of fichiers) {
								if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier))) {
									await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier), path.normalize(chemin + '/' + code + '/fichiers/' + fichier, { overwrite: true }))
								}
							}
							const archiveId = Math.floor((Math.random() * 100000) + 1)
							const sortie = fs.createWriteStream(path.normalize(chemin + '/' + code + '_' + archiveId + '.zip'))
							const archive = archiver('zip', {
								zlib: { level: 9 }
							})
							sortie.on('finish', async function () {
								await fs.remove(path.normalize(chemin + '/' + code))
								res.send(code + '_' + archiveId + '.zip')
							})
							archive.pipe(sortie)
							archive.directory(path.normalize(chemin + '/' + code), false)
							archive.finalize()
						} else {
							res.send('erreur')
						}
					} else if (parametres.hasOwnProperty('motdepasse') && parametres.motdepasse === '') {
						const resultat = await db.EXISTS('utilisateurs:' + identifiant)
						if (resultat === null) { res.send('erreur'); return false }
						if (resultat === 1) {
							let utilisateur = await db.HGETALL('utilisateurs:' + identifiant)
							utilisateur = Object.assign({}, utilisateur)
							if (utilisateur === null) { res.send('erreur'); return false }
							if (motdepasse.trim() !== '' && utilisateur.hasOwnProperty('motdepasse') && utilisateur.motdepasse.trim() !== '' && await bcrypt.compare(motdepasse, utilisateur.motdepasse)) {
								const chemin = path.join(__dirname, '..', '/static/temp')
								await fs.mkdirp(path.normalize(chemin + '/' + code))
								await fs.mkdirp(path.normalize(chemin + '/' + code + '/fichiers'))
								await fs.writeFile(path.normalize(chemin + '/' + code + '/donnees.json'), JSON.stringify(parametres, '', 4), 'utf8')
								const donnees = JSON.parse(parametres.donnees)
								if (Object.keys(donnees).length > 0) {
									const fichiers = []
									if (Object.keys(donnees.support).length > 0) {
										if (donnees.support.hasOwnProperty('fichier')) {
											fichiers.push(donnees.support.fichier)
										} else if (donnees.support.hasOwnProperty('image')) {
											fichiers.push(donnees.support.image)
										}
									}
									if (parametres.type === 'Sondage' || parametres.type === 'Questionnaire') {
										if (donnees.hasOwnProperty('questions')) {
											donnees.questions.forEach(function (q) {
												if (Object.keys(q.support).length > 0) {
													if (q.support.hasOwnProperty('fichier')) {
														fichiers.push(q.support.fichier)
													} else if (q.support.hasOwnProperty('image')) {
														fichiers.push(q.support.image)
													}
												}
												q.items.forEach(function (item) {
													if (item.image !== '') {
														fichiers.push(item.image)
													}
												})
											})
										} else {
											donnees.items.forEach(function (item) {
												if (item.image !== '') {
													fichiers.push(item.image)
												}
											})
										}
									} else if (parametres.type === 'Remue-méninges') {
										donnees.categories.forEach(function (categorie) {
											if (categorie.image !== '') {
												fichiers.push(categorie.image)
											}
										})
									}
									for (const fichier of fichiers) {
										if (await fs.pathExists(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier))) {
											await fs.copy(path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier), path.normalize(chemin + '/' + code + '/fichiers/' + fichier, { overwrite: true }))
										}
									}
									const archiveId = Math.floor((Math.random() * 100000) + 1)
									const sortie = fs.createWriteStream(path.normalize(chemin + '/' + code + '_' + archiveId + '.zip'))
									const archive = archiver('zip', {
										zlib: { level: 9 }
									})
									sortie.on('finish', async function () {
										await fs.remove(path.normalize(chemin + '/' + code))
										res.send(code + '_' + archiveId + '.zip')
									})
									archive.pipe(sortie)
									archive.directory(path.normalize(chemin + '/' + code), false)
									archive.finalize()
								} else {
									res.send('erreur')
								}
							} else {
								res.send('non_autorise')
							}
						} else {
							res.send('erreur')
						}
					} else {
						res.send('non_autorise')
					}
				} else {
					res.send('contenu_inexistant')
				}
			} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'supprimer') {
				const identifiant = req.body.identifiant
				const motdepasse = req.body.motdepasse
				const code = parseInt(req.body.id)
				const reponse = await db.EXISTS('interactions:' + code)
				if (reponse === null) { res.send('erreur'); return false }
				if (reponse === 1) {
					let donnees = await db.HGETALL('interactions:' + code)
					donnees = Object.assign({}, donnees)
					if (donnees === null) { res.send('erreur'); return false }
					if (donnees.hasOwnProperty('motdepasse') && motdepasse === donnees.motdepasse) {
						await db.DEL('interactions:' + code)
						await fs.remove(path.join(__dirname, '..', '/static/fichiers/' + code))
						res.send('contenu_supprime')
					} else if (donnees.hasOwnProperty('motdepasse') && donnees.motdepasse === '') {
						const resultat = await db.EXISTS('utilisateurs:' + identifiant)
						if (resultat === null) { res.send('erreur'); return false }
						if (resultat === 1) {
							let utilisateur = await db.HGETALL('utilisateurs:' + identifiant)
							utilisateur = Object.assign({}, utilisateur)
							if (utilisateur === null) { res.send('erreur'); return false }
							if (motdepasse.trim() !== '' && utilisateur.hasOwnProperty('motdepasse') && utilisateur.motdepasse.trim() !== '' && await bcrypt.compare(motdepasse, utilisateur.motdepasse)) {
								await db
								.multi()
								.DEL('interactions:' + code)
								.SREM('interactions-creees:' + identifiant, code.toString())
								.exec()
								await fs.remove(path.join(__dirname, '..', '/static/fichiers/' + code))
								res.send('contenu_supprime')
							} else {
								res.send('non_autorise')
							}
						} else {
							res.send('erreur')
						}
					} else {
						res.send('non_autorise')
					}
				} else {
					res.send('contenu_supprime')
				}
			} else {
				res.send('erreur')
			}
		}).catch(function () {
			res.send('erreur')
		})
	})

	app.post('/api/ladigitale/importer', function (req, res) {
		televerserArchive(req, res, async function (err) {
			if (err) { res.send('erreur'); return false }
			try {
				const tokenApi = req.body.token
				const domaine = req.headers.host
				const lien = req.body.lien
				const params = new URLSearchParams()
				params.append('token', tokenApi)
				params.append('domaine', domaine)
				axios.post(lien, params).then(async function (reponse) {
					if (reponse.data === 'non_autorise' || reponse.data === 'erreur') {
						res.send('erreur_token')
					} else if (reponse.data === 'token_autorise' && req.body.action && req.body.action === 'importer') {
						const titre = req.body.titre
						const motdepasse = req.body.motdepasse
						const source = path.join(__dirname, '..', '/static/temp/' + req.file.filename)
						const cible = path.join(__dirname, '..', '/static/temp/archive-' + Math.floor((Math.random() * 100000) + 1))
						await extract(source, { dir: cible })
						const donnees = await fs.readJson(path.normalize(cible + '/donnees.json'))
						const parametres = JSON.parse(req.body.parametres)
						// Vérification des clés des données
						if (donnees.hasOwnProperty('type') && donnees.hasOwnProperty('titre') && donnees.hasOwnProperty('code') && donnees.hasOwnProperty('motdepasse') && donnees.hasOwnProperty('donnees') && donnees.hasOwnProperty('reponses') && donnees.hasOwnProperty('sessions') && donnees.hasOwnProperty('statut') && donnees.hasOwnProperty('session') && donnees.hasOwnProperty('date')) {
							const code = Math.floor(1000000 + Math.random() * 9000000)
							const date = dayjs().format()
							const reponse = await db.EXISTS('interactions:' + code)
							if (reponse === null) { res.send('erreur'); return false }
							if (reponse === 0) {
								if (parametres.resultats === true) {
									await db.HSET('interactions:' + code, ['type', donnees.type, 'titre', titre, 'code', code, 'motdepasse', motdepasse, 'donnees', donnees.donnees, 'reponses', donnees.reponses, 'sessions', donnees.sessions, 'statut', '', 'session', donnees.session, 'date', date, 'digidrive', 1])
								} else {
									await db.HSET('interactions:' + code, ['type', donnees.type, 'titre', titre, 'code', code, 'motdepasse', motdepasse, 'donnees', donnees.donnees, 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, 'digidrive', 1])
								}
								const chemin = path.join(__dirname, '..', '/static/fichiers/' + code)
								await fs.move(path.normalize(cible + '/fichiers'), chemin)
								if (parametres.resultats === true) {
									res.send(code.toString())
								} else {
									res.send(code.toString())
								}
							} else {
								res.send('erreur')
							}
						} else {
							await fs.remove(source)
							await fs.remove(cible)
							res.send('donnees_corrompues')
						}
					}
				})
			} catch (err) {
				await fs.remove(path.join(__dirname, '..', '/static/temp/' + req.file.filename))
				res.send('erreur')
			}
		})
	})

	app.use(function (req, res) {
		res.redirect('/')
	})

	const port = process.env.PORT || 3000
	httpServer.listen(port)

	const io = new Server(httpServer, {
		// wsEngine: eiows.Server,
		pingInterval: 95000,
    	pingTimeout: 100000,
    	maxHttpBufferSize: 1e8,
		cookie: false,
		perMessageDeflate: false
	})
	if (cluster === true) {
		io.adapter(createAdapter())
	}
	const wrap = middleware => (socket, next) => middleware(socket.request, {}, next)
	io.use(wrap(sessionMiddleware))

	io.on('connection', function (socket) {
		socket.on('connexion', async function (donnees) {
			const code = donnees.code
			const identifiant = donnees.identifiant
			let nom = donnees.nom
			if (donnees.nomAleatoire === true) {
				const generateur = new NameForgeJS()
				const noms = generateur.generateNames()
				nom = noms[0].replace(/(^\w{1})|(\s+\w{1})/g, lettre => lettre.toUpperCase())
				socket.request.session.nom = nom
				socket.request.session.save()
			}
			socket.data.identifiant = identifiant
			socket.data.nom = nom
			socket.join(code)
			const clients = await io.to(code).fetchSockets()
			let utilisateurs = []
			for (let i = 0; i < clients.length; i++) {
				utilisateurs.push({ identifiant: clients[i].data.identifiant, nom: clients[i].data.nom })
			}
			utilisateurs = utilisateurs.filter((valeur, index, self) =>
				index === self.findIndex((t) => (
					t.identifiant === valeur.identifiant && t.nom === valeur.nom
				))
			)
			io.to(code).emit('connexion', utilisateurs)
		})

		socket.on('deconnexion', function (code) {
			socket.leave(code)
			socket.to(code).emit('deconnexion', socket.request.session.identifiant)
		})

		socket.on('interactionouverte', function (donnees) {
			socket.to(donnees.code).emit('interactionouverte', donnees)
		})

		socket.on('interactionenattente', function (donnees) {
			socket.to(donnees.code).emit('interactionenattente', donnees)
		})

		socket.on('interactionverrouillee', function (code) {
			socket.to(code).emit('interactionverrouillee')
		})

		socket.on('interactiondeverrouillee', function (code) {
			socket.to(code).emit('interactiondeverrouillee')
		})

		socket.on('interactionfermee', function (code) {
			socket.to(code).emit('interactionfermee')
		})

		socket.on('nuageaffiche', function (donnees) {
			socket.to(donnees.code).emit('nuageaffiche', donnees.reponses)
		})

		socket.on('nuagemasque', function (code) {
			socket.to(code).emit('nuagemasque')
		})

		socket.on('questionsuivante', function (donnees) {
			socket.to(donnees.code).emit('questionsuivante', donnees)
		})

		socket.on('classement', function (code, donnees) {
			socket.to(code).emit('classement', donnees)
		})

		socket.on('modifiernom', function (donnees) {
			socket.to(donnees.code).emit('modifiernom', donnees)
			socket.request.session.nom = donnees.nom
			socket.request.session.save()
		})

		socket.on('reponse', async function (reponse) {
			const code = parseInt(reponse.code)
			const session = parseInt(reponse.session)
			const rep = await db.EXISTS('interactions:' + code)
			if (rep === null) { socket.emit('erreur'); return false }
			if (rep === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { socket.emit('erreur'); return false }
				const type = resultat.type
				let reponses = JSON.parse(resultat.reponses)
				if (!reponses[session]) {
					reponses[session] = []
				}
				if (type === 'Sondage') {
					if (reponses[session].map(function (e) { return e.identifiant }).includes(reponse.donnees.identifiant) === true) {
						reponses[session].forEach(function (item) {
							if (item.identifiant === reponse.donnees.identifiant) {
								item.reponse = reponse.donnees.reponse
								if (item.nom !== reponse.donnees.nom && reponse.donnees.nom !== '') {
									item.nom = reponse.donnees.nom
								}
							}
						})
					} else {
						reponses[session].push(reponse.donnees)
					}
				} else if (type === 'Questionnaire') {
					if (reponses[session].map(function (e) { return e.identifiant }).includes(reponse.donnees.identifiant) === true) {
						reponses[session].forEach(function (item) {
							if (item.identifiant === reponse.donnees.identifiant) {
								item.reponse = reponse.donnees.reponse
								if (reponse.donnees.hasOwnProperty('temps')) {
									item.temps = reponse.donnees.temps
								}
								if (item.nom !== reponse.donnees.nom && reponse.donnees.nom !== '') {
									item.nom = reponse.donnees.nom
								}
							}
						})
					} else {
						reponses[session].push(reponse.donnees)
					}
				} else if (type === 'Remue-méninges' || type === 'Nuage-de-mots') {
					reponses[session].push(reponse.donnees)
				}
				await db.HSET('interactions:' + code, 'reponses', JSON.stringify(reponses))
				socket.to(reponse.code).emit('reponse', reponse)
				socket.emit('reponseenvoyee', reponse)
				let reponsesSession = []
				const donneesSession = []
				let scoreTotal = 0
				if (socket.request.session.identifiant === reponse.donnees.identifiant) {
					reponses[session].forEach(function (item) {
						if (item.identifiant === reponse.donnees.identifiant) {
							reponsesSession.push(item)
						}
					})
					if (type === 'Questionnaire' && reponsesSession[0] && reponsesSession[0].reponse) {
						const donnees = JSON.parse(resultat.donnees)
						reponsesSession[0].reponse.forEach(function (item, index) {
							const question = donnees.questions[index]
							const reponseCorrecte = definirReponseCorrecte(question, item).reponseCorrecte
							let itemsCorrects = []
							if (donnees.options.reponses === true || donnees.options.reponses === 'oui') {
								itemsCorrects = definirReponseCorrecte(question, item).itemsCorrects
							} else if (donnees.options.reponses === 'utilisateur') {
								itemsCorrects = definirReponseCorrecte(question, item).itemsCorrects
								itemsCorrects = itemsCorrects.filter(function (element) {
									return item.includes(element)
								})
							}
							let retroaction = ''
							if (donnees.options.retroaction === true && reponseCorrecte && question.hasOwnProperty('retroaction') && question.retroaction.correcte !== '') {
								retroaction = question.retroaction.correcte
							} else if (donnees.options.retroaction === true && !reponseCorrecte && question.hasOwnProperty('retroaction') && question.retroaction.incorrecte !== '') {
								retroaction = question.retroaction.incorrecte
							}
							donneesSession.push({ reponseCorrecte: reponseCorrecte, itemsCorrects: itemsCorrects, retroaction: retroaction })
						})
						scoreTotal = calculerScoreTotal(reponsesSession[0], donnees.options, donnees.questions)
					}
				} else {
					reponsesSession = reponses[session]
				}
				socket.emit('reponses', { code: reponse.code, session: reponse.session, reponsesSession: reponsesSession, donneesSession: donneesSession, scoreTotal: scoreTotal })
				socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
				socket.request.session.save()
			} else {
				socket.emit('erreurcode')
			}
		})

		socket.on('supprimermessage', async function (donnees) {
			const code = parseInt(donnees.code)
			const session = parseInt(donnees.session)
			const id = donnees.id
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { socket.emit('erreur'); return false }
				let reponses = JSON.parse(resultat.reponses)
				if (reponses[session]) {
					reponses[session].forEach(function (item) {
						if (item.reponse.id === id) {
							item.reponse.visible = false
						}
					})
					await db.HSET('interactions:' + code, 'reponses', JSON.stringify(reponses))
					io.to(donnees.code).emit('reponses', { code: donnees.code, session: donnees.session, reponsesSession: reponses[session] })
				}
			} else {
				socket.emit('erreurcode')
			}
		})

		socket.on('reorganisermessages', async function (donnees) {
			const code = parseInt(donnees.code)
			const session = parseInt(donnees.session)
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { socket.emit('erreur'); return false }
				let reponses = JSON.parse(resultat.reponses)
				if (reponses[session]) {
					reponses[session] = donnees.reponses
					await db.HSET('interactions:' + code, 'reponses', JSON.stringify(reponses))
					io.to(donnees.code).emit('reponses', { code: donnees.code, session: donnees.session, reponsesSession: reponses[session] })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			} else {
				socket.emit('erreurcode')
			}
		})

		socket.on('modifiercouleurmot', async function (donnees) {
			const code = parseInt(donnees.code)
			const session = parseInt(donnees.session)
			const mot = donnees.mot
			const couleur = donnees.couleur
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { socket.emit('erreur'); return false }
				let reponses = JSON.parse(resultat.reponses)
				if (reponses[session]) {
					reponses[session].forEach(function (item) {
						if (item.reponse.texte === mot) {
							item.reponse.couleur = couleur
						}
					})
					await db.HSET('interactions:' + code, 'reponses', JSON.stringify(reponses))
					socket.emit('modifiercouleurmot', { code: donnees.code, session: donnees.session, reponsesSession: reponses[session] })
					socket.request.session.cookie.expires = new Date(Date.now() + dureeSession)
					socket.request.session.save()
				}
			} else {
				socket.emit('erreurcode')
			}
		})

		socket.on('supprimermot', async function (donnees) {
			const code = parseInt(donnees.code)
			const session = parseInt(donnees.session)
			const mot = donnees.mot
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { socket.emit('erreur'); return false }
				let reponses = JSON.parse(resultat.reponses)
				if (reponses[session]) {
					reponses[session].forEach(function (item) {
						if (item.reponse.texte === mot) {
							item.reponse.visible = false
						}
					})
					await db.HSET('interactions:' + code, 'reponses', JSON.stringify(reponses))
					io.to(donnees.code).emit('reponses', { code: donnees.code, session: donnees.session, reponsesSession: reponses[session] })
				}
			} else {
				socket.emit('erreurcode')
			}
		})

		socket.on('supprimermots', async function (donnees) {
			const code = parseInt(donnees.code)
			const session = parseInt(donnees.session)
			const mots = donnees.mots
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { socket.emit('erreur'); return false }
				let reponses = JSON.parse(resultat.reponses)
				if (reponses[session]) {
					reponses[session].forEach(function (item) {
						if (mots.includes(item.reponse.texte) === true) {
							item.reponse.visible = false
						}
					})
					await db.HSET('interactions:' + code, 'reponses', JSON.stringify(reponses))
					socket.to(donnees.code).emit('reponses', { code: donnees.code, session: donnees.session, reponsesSession: reponses[session] })
				}
			} else {
				socket.emit('erreurcode')
			}
		})

		socket.on('recupererdonneesnuage', async function (donnees) {
			const code = parseInt(donnees.code)
			const session = parseInt(donnees.session)
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				let resultat = await db.HGETALL('interactions:' + code)
				resultat = Object.assign({}, resultat)
				if (resultat === null) { socket.emit('erreur'); return false }
				let reponses = JSON.parse(resultat.reponses)
				if (reponses[session]) {
					socket.emit('nuageaffiche', reponses[session])
				}
			} else {
				socket.emit('erreurcode')
			}
		})

		socket.on('modifierlangue', function (langue) {
			socket.request.session.langue = langue
			socket.request.session.save()
		})
	})

	async function envoyerPage (pageContextInit, res, next) {
		const pageContext = await renderPage(pageContextInit)
		const { httpResponse } = pageContext
		if (!httpResponse) {
			return next()
		}
		const { body, statusCode, headers, earlyHints } = httpResponse
		if (earlyHints103 === true && res.writeEarlyHints) {
			res.writeEarlyHints({ link: earlyHints.map((e) => e.earlyHintLink) })
		}
		if (headers) {
			headers.forEach(([name, value]) => res.setHeader(name, value))
		}
		res.status(statusCode).send(body)
	}

	async function verifierAcces (req, code, identifiant, motdepasse) {
		return new Promise(async function (resolve) {
			const reponse = await db.EXISTS('interactions:' + code)
			if (reponse === null) { resolve('erreur'); return false }
			if (reponse === 1) {
				let donnees = await db.HGETALL('interactions:' + code)
				donnees = Object.assign({}, donnees)
				if (donnees === null) { resolve('erreur'); return false }
				if (donnees.hasOwnProperty('motdepasse') && motdepasse !== '' && motdepasse === donnees.motdepasse) {
					req.session.identifiant = identifiant
					req.session.nom = ''
					req.session.email = ''
					if (req.session.langue === '' || req.session.langue === undefined) {
						req.session.langue = 'fr'
					}
					req.session.role = 'auteur'
					req.session.cookie.expires = new Date(Date.now() + dureeSession)
					if (!req.session.hasOwnProperty('interactions')) {
						req.session.interactions = []
					}
					if (!req.session.interactions.map(item => item.code).includes(code)) {
						req.session.interactions.push({ code: code, motdepasse: motdepasse })
					}
					if (!donnees.hasOwnProperty('digidrive') || (donnees.hasOwnProperty('digidrive') && parseInt(donnees.digidrive) === 0)) {
						await db.HSET('interactions:' + code, 'digidrive', 1)
					}
					resolve('interaction_debloquee')
				} else if (identifiant === donnees.identifiant && donnees.hasOwnProperty('motdepasse') && donnees.motdepasse === '') {
					const resultat = await db.EXISTS('utilisateurs:' + identifiant)
					if (resultat === null) { resolve('erreur'); return false }
					if (resultat === 1) {
						let utilisateur = await db.HGETALL('utilisateurs:' + identifiant)
						utilisateur = Object.assign({}, utilisateur)
						if (utilisateur === null) { resolve('erreur'); return false }
						if (motdepasse.trim() !== '' && utilisateur.hasOwnProperty('motdepasse') && utilisateur.motdepasse.trim() !== '' && await bcrypt.compare(motdepasse, utilisateur.motdepasse)) {
							req.session.identifiant = identifiant
							req.session.nom = utilisateur.nom
							req.session.email = ''
							req.session.langue = utilisateur.langue
							req.session.role = 'auteur'
							req.session.cookie.expires = new Date(Date.now() + dureeSession)
							if (!req.session.hasOwnProperty('interactions')) {
								req.session.interactions = []
							}
							if (!req.session.interactions.map(item => item.code).includes(code)) {
								req.session.interactions.push({ code: code, motdepasse: motdepasse })
							}
							if (!donnees.hasOwnProperty('digidrive') || (donnees.hasOwnProperty('digidrive') && parseInt(donnees.digidrive) === 0)) {
								await db.HSET('interactions:' + code, 'digidrive', 1)
							}
							resolve('interaction_debloquee')
						} else {
							resolve('erreur')
						}
					} else {
						resolve('erreur')
					}
				} else {
					resolve('erreur')
				}
			} else {
				resolve('erreur')
			}
		})
	}

	function creerMotDePasse () {
		let motdepasse = ''
		const lettres = 'abcdefghijklmnopqrstuvwxyz1234567890'
		for (let i = 0; i < 6; i++) {
			motdepasse += lettres.charAt(Math.floor(Math.random() * 36))
		}
		return motdepasse
	}

	function formaterDate (date, mot, langue) {
		let dateFormattee = ''
		switch (langue) {
			case 'fr':
				dateFormattee = mot + ' le ' + date
				break
			case 'en':
				dateFormattee = mot + ' on ' + date
				break
			case 'es':
				dateFormattee = mot + ' el ' + date
				break
			case 'it':
				dateFormattee = mot + ' il ' + date
				break
			case 'de':
				dateFormattee = mot + ' am ' + date
				break
		}
		return dateFormattee
	}

	function definirReponseCorrecte (question, reponse) {
		const itemsCorrects = []
		const bonnesReponses = []
		const mauvaisesReponses = []
		if (question.option !== 'texte-court') {
			question.items.forEach(function (item) {
				if (item.reponse === true && item.texte !== '') {
					itemsCorrects.push(item.texte)
				} else if (item.reponse === true && item.image !== '') {
					itemsCorrects.push(item.image)
				}
			})
			question.items.forEach(function (item) {
				if (item.reponse === true && (reponse.includes(item.texte) || reponse.includes(item.image))) {
					bonnesReponses.push(item)
				} else if (item.reponse === false && (reponse.includes(item.texte) || reponse.includes(item.image))) {
					mauvaisesReponses.push(item)
				}
			})
		} else {
			const reponsesTexte = question.reponses.split(',')
			reponsesTexte.forEach(function (item, index) {
				reponsesTexte[index] = item.trim()
			})
			itemsCorrects.push(...reponsesTexte)
		}
		if ((question.option === 'choix-unique' && bonnesReponses.length > 0) || (question.option === 'choix-multiples' && itemsCorrects.every(i => reponse.includes(i)) === true && mauvaisesReponses.length === 0) || (question.option === 'texte-court' && itemsCorrects.includes(reponse.toString().trim()) === true && mauvaisesReponses.length === 0)) {
			return { reponseCorrecte: true, itemsCorrects: itemsCorrects }
		} else {
			return { reponseCorrecte: false, itemsCorrects: itemsCorrects }
		}
	}

	function calculerScoreTotal (reponsesSession, options, questions) {
		let score = 0
		let scoreTemps = []
		let temps = []
		const reponses = reponsesSession.reponse
		if (reponsesSession.hasOwnProperty('score') && options.points !== 'classique') {
			scoreTemps = reponsesSession.score
		}
		if (reponsesSession.hasOwnProperty('temps') && options.points !== 'classique') {
			temps = reponsesSession.temps
		}
		if (scoreTemps.length > 0) {
			scoreTemps.forEach(function (points) {
				score = score + points
			})
		} else if (reponses.length > 0) {
			questions.forEach(function (question, indexQuestion) {
				if (reponses[indexQuestion]) {
					const reponseCorrecte = []
					const bonnesReponses = []
					const mauvaisesReponses = []
					if (question.option !== 'texte-court') {
						question.items.forEach(function (item) {
							if (item.reponse === true && item.texte !== '') {
								reponseCorrecte.push(item.texte)
							} else if (item.reponse === true && item.image !== '') {
								reponseCorrecte.push(item.image)
							}
						})
						question.items.forEach(function (item) {
							if (item.reponse === true && (reponses[indexQuestion].includes(item.texte) || reponses[indexQuestion].includes(item.image))) {
								bonnesReponses.push(item)
							} else if (item.reponse === false && (reponses[indexQuestion].includes(item.texte) || reponses[indexQuestion].includes(item.image))) {
								mauvaisesReponses.push(item)
							}
						})
					} else {
						const reponsesTexte = question.reponses.split(',')
						reponsesTexte.forEach(function (item, index) {
							reponsesTexte[index] = item.trim()
						})
						if (reponsesTexte.includes(reponses[indexQuestion].toString().trim()) === true) {
							bonnesReponses.push(reponses[indexQuestion].toString())
						}
						reponseCorrecte.push(question.reponses)
					}
					let multiplicateurSecondes = 10
					if (options.hasOwnProperty('multiplicateur') && options.multiplicateur > 0) {
						multiplicateurSecondes = options.multiplicateur
					}
					if (((question.option === 'choix-unique' && bonnesReponses.length > 0) || (question.option === 'texte-court' && bonnesReponses.length > 0) || (question.option === 'choix-multiples' && reponseCorrecte.every(i => reponses[indexQuestion].includes(i)) === true && mauvaisesReponses.length === 0)) && options.points === 'classique' && question.hasOwnProperty('points')) {
						score = score + question.points
					} else if (((question.option === 'choix-unique' && bonnesReponses.length > 0) || (question.option === 'texte-court' && bonnesReponses.length > 0) || (question.option === 'choix-multiples' && reponseCorrecte.every(i => reponses[indexQuestion].includes(i)) === true && mauvaisesReponses.length === 0)) && options.points === 'classique' && !question.hasOwnProperty('points')) {
						score = score + 1000
					} else if (((question.option === 'choix-unique' && bonnesReponses.length > 0) || (question.option === 'texte-court' && bonnesReponses.length > 0) || (question.option === 'choix-multiples' && reponseCorrecte.every(i => reponses[indexQuestion].includes(i)) === true && mauvaisesReponses.length === 0)) && options.points !== 'classique' && question.hasOwnProperty('points')) {
						score = score + Math.round(question.points - (temps[indexQuestion] * multiplicateurSecondes))
					} else if (((question.option === 'choix-unique' && bonnesReponses.length > 0) || (question.option === 'texte-court' && bonnesReponses.length > 0) || (question.option === 'choix-multiples' && reponseCorrecte.every(i => reponses[indexQuestion].includes(i)) === true && mauvaisesReponses.length === 0)) && options.points !== 'classique' && !question.hasOwnProperty('points')) {
						score = score + Math.round(1000 - (temps[indexQuestion] * multiplicateurSecondes))
					} else if ((bonnesReponses.length - mauvaisesReponses.length) > 0 && options.points === 'classique' && question.hasOwnProperty('points')) {
						score = score + ((question.points / reponseCorrecte.length) * (bonnesReponses.length - mauvaisesReponses.length))
					} else if ((bonnesReponses.length - mauvaisesReponses.length) > 0 && options.points === 'classique' && !question.hasOwnProperty('points')) {
						score = score + ((1000 / reponseCorrecte.length) * (bonnesReponses.length - mauvaisesReponses.length))
					} else if ((bonnesReponses.length - mauvaisesReponses.length) > 0 && options.points !== 'classique' && question.hasOwnProperty('points')) {
						score = score + ((Math.round(question.points - (temps[indexQuestion] * multiplicateurSecondes)) / reponseCorrecte.length) * (bonnesReponses.length - mauvaisesReponses.length))
					} else if ((bonnesReponses.length - mauvaisesReponses.length) > 0 && options.points !== 'classique' && !question.hasOwnProperty('points')) {
						score = score + ((Math.round(1000 - (temps[indexQuestion] * multiplicateurSecondes)) / reponseCorrecte.length) * (bonnesReponses.length - mauvaisesReponses.length))
					} else {
						score = score + 0
					}
				}
			})
		}
		return score
	}

	function definirStatistiquesSondage (question, reponses) {
		const personnes = []
		const pourcentages = []
		if (question.option !== 'texte-court') {
			for (let i = 0; i < question.items.length; i++) {
				personnes.push(0)
				pourcentages.push(0)
			}
			question.items.forEach(function (item, index) {
				let total = 0
				let nombreReponses = 0
				reponses.forEach(function (donnees) {
					donnees.reponse.forEach(function (reponse) {
						if (reponse === item.texte || reponse === item.image) {
							nombreReponses++
						}
						total++
					})
				})
				if (nombreReponses > 0) {
					personnes[index] = nombreReponses
					const pourcentage = (nombreReponses / total) * 100
					pourcentages[index] = Math.round(pourcentage)
				}
			})
		} else {
			let items = []
			reponses.forEach(function (donnees) {
				donnees.reponse.forEach(function (reponse) {
					if (!items.includes(reponse.toString().trim())) {
						items.push(reponse.toString().trim())
					}
				})
			})
			for (let i = 0; i < items.length; i++) {
				personnes.push(0)
				pourcentages.push(0)
			}
			items.forEach(function (item, index) {
				let total = 0
				let nombreReponses = 0
				reponses.forEach(function (donnees) {
					donnees.reponse.forEach(function (reponse) {
						if (item === reponse.toString().trim()) {
							nombreReponses++
						}
						total++
					})
				})
				if (nombreReponses > 0) {
					personnes[index] = nombreReponses
					const pourcentage = (nombreReponses / total) * 100
					pourcentages[index] = Math.round(pourcentage)
				}
			})
		}
		return { personnes: personnes, pourcentages: pourcentages }
	}

	function definirMessagesRemueMeninges (categories, reponses) {
		const messagesVisibles = []
		const messagesSupprimes = []
		for (let i = 0; i < categories.length; i++) {
			messagesVisibles.push([])
			messagesSupprimes.push([])
		}
		if (messagesVisibles.length > 0) {
			reponses.forEach(function (item) {
				let index = -1
				categories.forEach(function (categorie, indexCategorie) {
					if (item.reponse.categorie === categorie.texte || item.reponse.categorie === categorie.image) {
						index = indexCategorie
					}
				})
				if (item.reponse.visible && index > -1) {
					messagesVisibles[index].push(item)
				} else if (index > -1) {
					messagesSupprimes[index].push(item)
				}
			})
		} else {
			reponses.forEach(function (item) {
				if (item.reponse.visible) {
					messagesVisibles.push(item)
				} else {
					messagesSupprimes.push(item)
				}
			})
		}
		return { visibles: messagesVisibles, supprimes: messagesSupprimes }
	}

	function definirMotsNuageDeMots (reponses) {
		const messagesVisibles = []
		const messagesSupprimes = []
		reponses.forEach(function (item) {
			if (item.reponse.visible) {
				messagesVisibles.push(item)
			} else {
				messagesSupprimes.push(item)
			}
		})
		return { visibles: messagesVisibles, supprimes: messagesSupprimes }
	}

	function definirStatistiquesQuestions (questions, reponses) {
		const statistiques = []
		questions.forEach(function (question, indexQuestion) {
			const personnes = []
			const pourcentages = []
			if (question.option !== 'texte-court') {
				for (let i = 0; i < question.items.length; i++) {
					personnes.push(0)
					pourcentages.push(0)
				}
				question.items.forEach(function (item, index) {
					let total = 0
					let nombreReponses = 0
					reponses.forEach(function (donnees) {
						donnees.reponse[indexQuestion].forEach(function (reponse) {
							if (reponse === item.texte || reponse === item.image) {
								nombreReponses++
							}
						})
						total++
					})
					if (nombreReponses > 0) {
						personnes[index] = nombreReponses
						const pourcentage = (nombreReponses / total) * 100
						pourcentages[index] = Math.round(pourcentage)
					}
				})
			} else {
				let items = []
				reponses.forEach(function (donnees) {
					donnees.reponse[indexQuestion].forEach(function (reponse) {
						if (!items.includes(reponse.toString().trim())) {
							items.push(reponse.toString().trim())
						}
					})
				})
				for (let i = 0; i < items.length; i++) {
					personnes.push(0)
					pourcentages.push(0)
				}
				items.forEach(function (item, index) {
					let total = 0
					let nombreReponses = 0
					reponses.forEach(function (donnees) {
						donnees.reponse[indexQuestion].forEach(function (reponse) {
							if (item === reponse.toString().trim()) {
								nombreReponses++
							}
							total++
						})
					})
					if (nombreReponses > 0) {
						personnes[index] = nombreReponses
						const pourcentage = (nombreReponses / total) * 100
						pourcentages[index] = Math.round(pourcentage)
					}
				})
			}
			statistiques.push({ personnes: personnes, pourcentages: pourcentages })
		})
		return statistiques
	}

	function definirReponses (reponses, indexQuestion) {
		let total = 0
		reponses.forEach(function (item) {
			if (item.hasOwnProperty('reponse') && item.reponse[indexQuestion].length > 0) {
				total++
			}
		})
		return total
	}

	function genererMotDePasse (longueur) {
		function rand (max) {
			return Math.floor(Math.random() * max)
		}
		function verifierMotDePasse (motdepasse, regex, caracteres) {
			if (!regex.test(motdepasse)) {
				const nouveauCaractere = caracteres.charAt(rand(caracteres.length))
				const position = rand(motdepasse.length + 1)
				motdepasse = motdepasse.slice(0, position) + nouveauCaractere + motdepasse.slice(position)
			}
			return motdepasse
		}
		const listeCaracteres = '123456789abcdefghijklmnopqrstuvwxyz'
		const caracteresSpeciaux = '!#$@*'
		const specialRegex = /[!#\$@*]/
		const majuscules = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		const majusculesRegex = /[A-Z]/

		const caracteres = listeCaracteres.split('')
		let motdepasse = ''
		let index

		while (motdepasse.length < longueur) {
			index = rand(caracteres.length)
			motdepasse += caracteres[index]
			caracteres.splice(index, 1)
		}
		motdepasse = verifierMotDePasse(motdepasse, specialRegex, caracteresSpeciaux)
		motdepasse = verifierMotDePasse(motdepasse, majusculesRegex, majuscules)
		return motdepasse  
	}

	function verifierEmail (email) {
		const regexExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/gi
		return regexExp.test(email)
	}

	const televerser = multer({
		storage: multer.diskStorage({
			destination: function (req, fichier, callback) {
				const code = req.body.code
				const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/')
				callback(null, chemin)
			},
			filename: function (req, fichier, callback) {
				const info = path.parse(fichier.originalname)
				const extension = info.ext.toLowerCase()
				let nom = v.latinise(info.name.toLowerCase())
				nom = nom.replace(/\ /gi, '-')
				nom = nom.replace(/[^0-9a-z_\-]/gi, '')
				if (nom.length > 100) {
					nom = nom.substring(0, 100)
				}
				nom = nom + '_' + Math.random().toString(36).substring(2) + extension
				callback(null, nom)
			}
		})
	}).single('fichier')

	const televerserArchive = multer({
		storage: multer.diskStorage({
			destination: function (req, fichier, callback) {
				const chemin = path.join(__dirname, '..', '/static/temp/')
				callback(null, chemin)
			},
			filename: function (req, fichier, callback) {
				const info = path.parse(fichier.originalname)
				const extension = info.ext.toLowerCase()
				let nom = v.latinise(info.name.toLowerCase())
				nom = nom.replace(/\ /gi, '-')
				nom = nom.replace(/[^0-9a-z_\-]/gi, '')
				if (nom.length > 100) {
					nom = nom.substring(0, 100)
				}
				nom = nom + '_' + Math.random().toString(36).substring(2) + extension
				callback(null, nom)
			}
		})
	}).single('fichier')

	function recupererDonnees (identifiant) {
		const donneesUtilisateur = new Promise(async function (resolve) {
			const filtre = 'date-desc'
			const reponse = await db.EXISTS('utilisateurs:' + identifiant)
			if (reponse === null) { resolve(filtre); return false }
			if (reponse === 1) {
				let donnees = await db.HGETALL('utilisateurs:' + identifiant)
				donnees = Object.assign({}, donnees)
				if (donnees.hasOwnProperty('filtre')) {
					resolve(donnees.filtre)
				} else {
					resolve(filtre)
				}
			} else {
				resolve(filtre)
			}
		})
		const donneesInteractionsCreees = new Promise(async function (resolveMain) {
			const interactions = await db.SMEMBERS('interactions-creees:' + identifiant)
			const donneeInteractions = []
			if (interactions === null) { resolveMain(donneeInteractions); return false }
			for (const interaction of interactions) {
				const donneeInteraction = new Promise(async function (resolve) {
					let donnees = await db.HGETALL('interactions:' + interaction)
					donnees = Object.assign({}, donnees)
					if (donnees === null) { resolve({}); return false }
					resolve(donnees)
				})
				donneeInteractions.push(donneeInteraction)
			}
			Promise.all(donneeInteractions).then(function (resultat) {
				resolveMain(resultat)
			})
		})
		return Promise.all([donneesInteractionsCreees, donneesUtilisateur])
	}

	async function supprimerFichier (code, fichier) {
		const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier)
		if (await fs.pathExists(chemin)) {
			await fs.remove(chemin)
		}
	}
}
