import en from './en.json'
import es from './es.json'
import fr from './fr.json'
import it from './it.json'
import de from './de.json'

export default {
	en,
	es,
	fr,
	it,
	de
}
